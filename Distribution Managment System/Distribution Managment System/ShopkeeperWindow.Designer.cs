﻿namespace Distribution_Managment_System
{
    partial class ShopkeeperWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.shop_datagrid = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.shop_landline_txtbox = new System.Windows.Forms.TextBox();
            this.region_lbl = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.shop_address_txtbox = new System.Windows.Forms.TextBox();
            this.phone_num_lbl = new System.Windows.Forms.Label();
            this.shop_region_combox = new System.Windows.Forms.ComboBox();
            this.vehicle_id_lbl = new System.Windows.Forms.Label();
            this.shop_name_txtbox = new System.Windows.Forms.TextBox();
            this.rider_name_lbl = new System.Windows.Forms.Label();
            this.shop_id_txtbox = new System.Windows.Forms.TextBox();
            this.rider_id_lbl = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.shopkeeper_existence = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.shopkeeper_combox = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.shopkeeper_name = new System.Windows.Forms.TextBox();
            this.ll = new System.Windows.Forms.Label();
            this.shopkeeper_email = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.shop_id_indicator_lblr = new System.Windows.Forms.Label();
            this.shop_name_indicator_lblr = new System.Windows.Forms.Label();
            this.shop_region_indicator_lblr = new System.Windows.Forms.Label();
            this.shop_landline_indicator_lblr = new System.Windows.Forms.Label();
            this.shop_address_indicator_lblr = new System.Windows.Forms.Label();
            this.shopkeeper_name_indicator_lbl = new System.Windows.Forms.Label();
            this.shopkeeper_email_indicator_lbl = new System.Windows.Forms.Label();
            this.shopkeeper_phone = new System.Windows.Forms.TextBox();
            this.oo = new System.Windows.Forms.Label();
            this.shopkeeper_phone_indicator_lbl = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button8 = new System.Windows.Forms.Button();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.shop_datagrid)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.shop_datagrid, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 86.97789F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.02211F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 357F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1449, 765);
            this.tableLayoutPanel1.TabIndex = 104;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // shop_datagrid
            // 
            this.shop_datagrid.AllowUserToAddRows = false;
            this.shop_datagrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.shop_datagrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.shop_datagrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.shop_datagrid.Location = new System.Drawing.Point(3, 410);
            this.shop_datagrid.Name = "shop_datagrid";
            this.shop_datagrid.ReadOnly = true;
            this.shop_datagrid.Size = new System.Drawing.Size(1443, 352);
            this.shop_datagrid.TabIndex = 147;
            this.shop_datagrid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.shop_datagrid_CellClick);
            this.shop_datagrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.shop_datagrid_CellContentClick_1);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 37.5F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 62.5F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 381F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 462F));
            this.tableLayoutPanel2.Controls.Add(this.shop_landline_txtbox, 1, 7);
            this.tableLayoutPanel2.Controls.Add(this.region_lbl, 0, 7);
            this.tableLayoutPanel2.Controls.Add(this.label6, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.shop_address_txtbox, 1, 10);
            this.tableLayoutPanel2.Controls.Add(this.phone_num_lbl, 0, 10);
            this.tableLayoutPanel2.Controls.Add(this.shop_region_combox, 1, 5);
            this.tableLayoutPanel2.Controls.Add(this.vehicle_id_lbl, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.shop_name_txtbox, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.rider_name_lbl, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.shop_id_txtbox, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.rider_id_lbl, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label5, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.shopkeeper_existence, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.label4, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.label10, 2, 3);
            this.tableLayoutPanel2.Controls.Add(this.shopkeeper_combox, 3, 3);
            this.tableLayoutPanel2.Controls.Add(this.label9, 2, 5);
            this.tableLayoutPanel2.Controls.Add(this.shopkeeper_name, 3, 5);
            this.tableLayoutPanel2.Controls.Add(this.ll, 2, 7);
            this.tableLayoutPanel2.Controls.Add(this.shopkeeper_email, 3, 7);
            this.tableLayoutPanel2.Controls.Add(this.panel3, 3, 11);
            this.tableLayoutPanel2.Controls.Add(this.shop_id_indicator_lblr, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.shop_name_indicator_lblr, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.shop_region_indicator_lblr, 1, 6);
            this.tableLayoutPanel2.Controls.Add(this.shop_landline_indicator_lblr, 1, 8);
            this.tableLayoutPanel2.Controls.Add(this.shop_address_indicator_lblr, 1, 11);
            this.tableLayoutPanel2.Controls.Add(this.shopkeeper_name_indicator_lbl, 3, 6);
            this.tableLayoutPanel2.Controls.Add(this.shopkeeper_email_indicator_lbl, 3, 8);
            this.tableLayoutPanel2.Controls.Add(this.shopkeeper_phone, 3, 9);
            this.tableLayoutPanel2.Controls.Add(this.oo, 2, 9);
            this.tableLayoutPanel2.Controls.Add(this.shopkeeper_phone_indicator_lbl, 3, 10);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 12;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.19608F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.803922F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.235294F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.31944F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 21.52778F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1443, 348);
            this.tableLayoutPanel2.TabIndex = 0;
            this.tableLayoutPanel2.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel2_Paint);
            // 
            // shop_landline_txtbox
            // 
            this.shop_landline_txtbox.ForeColor = System.Drawing.Color.Gray;
            this.shop_landline_txtbox.Location = new System.Drawing.Point(228, 176);
            this.shop_landline_txtbox.Name = "shop_landline_txtbox";
            this.shop_landline_txtbox.Size = new System.Drawing.Size(287, 20);
            this.shop_landline_txtbox.TabIndex = 121;
            this.shop_landline_txtbox.Text = "Land-Line";
            this.shop_landline_txtbox.TextChanged += new System.EventHandler(this.shop_landline_txtbox_TextChanged);
            this.shop_landline_txtbox.Enter += new System.EventHandler(this.shop_landline_txtbox_Enter);
            // 
            // region_lbl
            // 
            this.region_lbl.AutoSize = true;
            this.region_lbl.Font = new System.Drawing.Font("Century Gothic", 12.75F);
            this.region_lbl.Location = new System.Drawing.Point(3, 173);
            this.region_lbl.Name = "region_lbl";
            this.region_lbl.Size = new System.Drawing.Size(90, 21);
            this.region_lbl.TabIndex = 122;
            this.region_lbl.Text = "Land-Line";
            this.region_lbl.Click += new System.EventHandler(this.region_lbl_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Black;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 19.8F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(228, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(176, 28);
            this.label6.TabIndex = 114;
            this.label6.Text = "Add A SHOP";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // shop_address_txtbox
            // 
            this.shop_address_txtbox.ForeColor = System.Drawing.Color.Gray;
            this.shop_address_txtbox.Location = new System.Drawing.Point(228, 252);
            this.shop_address_txtbox.Multiline = true;
            this.shop_address_txtbox.Name = "shop_address_txtbox";
            this.shop_address_txtbox.Size = new System.Drawing.Size(287, 35);
            this.shop_address_txtbox.TabIndex = 123;
            this.shop_address_txtbox.Text = "Address";
            this.shop_address_txtbox.TextChanged += new System.EventHandler(this.shop_address_txtbox_TextChanged);
            this.shop_address_txtbox.Enter += new System.EventHandler(this.shop_address_txtbox_Enter);
            // 
            // phone_num_lbl
            // 
            this.phone_num_lbl.AutoSize = true;
            this.phone_num_lbl.Font = new System.Drawing.Font("Century Gothic", 12.75F);
            this.phone_num_lbl.Location = new System.Drawing.Point(3, 249);
            this.phone_num_lbl.Name = "phone_num_lbl";
            this.phone_num_lbl.Size = new System.Drawing.Size(77, 21);
            this.phone_num_lbl.TabIndex = 124;
            this.phone_num_lbl.Text = "Address";
            this.phone_num_lbl.Click += new System.EventHandler(this.phone_num_lbl_Click_1);
            // 
            // shop_region_combox
            // 
            this.shop_region_combox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.shop_region_combox.FormattingEnabled = true;
            this.shop_region_combox.Items.AddRange(new object[] {
            "Lahore",
            "kasur ",
            "Gujranwala",
            "jhang",
            "khusbab",
            "Mainwali "});
            this.shop_region_combox.Location = new System.Drawing.Point(228, 127);
            this.shop_region_combox.Name = "shop_region_combox";
            this.shop_region_combox.Size = new System.Drawing.Size(287, 21);
            this.shop_region_combox.TabIndex = 119;
            this.shop_region_combox.SelectedIndexChanged += new System.EventHandler(this.shop_region_combox_SelectedIndexChanged);
            // 
            // vehicle_id_lbl
            // 
            this.vehicle_id_lbl.AutoSize = true;
            this.vehicle_id_lbl.Font = new System.Drawing.Font("Century Gothic", 12.75F);
            this.vehicle_id_lbl.Location = new System.Drawing.Point(3, 124);
            this.vehicle_id_lbl.Name = "vehicle_id_lbl";
            this.vehicle_id_lbl.Size = new System.Drawing.Size(66, 21);
            this.vehicle_id_lbl.TabIndex = 120;
            this.vehicle_id_lbl.Text = "Region";
            this.vehicle_id_lbl.Click += new System.EventHandler(this.vehicle_id_lbl_Click);
            // 
            // shop_name_txtbox
            // 
            this.shop_name_txtbox.ForeColor = System.Drawing.Color.Gray;
            this.shop_name_txtbox.Location = new System.Drawing.Point(228, 79);
            this.shop_name_txtbox.Name = "shop_name_txtbox";
            this.shop_name_txtbox.Size = new System.Drawing.Size(287, 20);
            this.shop_name_txtbox.TabIndex = 117;
            this.shop_name_txtbox.Text = "Name";
            this.shop_name_txtbox.TextChanged += new System.EventHandler(this.shop_name_txtbox_TextChanged);
            this.shop_name_txtbox.Enter += new System.EventHandler(this.shop_name_txtbox_Enter);
            // 
            // rider_name_lbl
            // 
            this.rider_name_lbl.AutoSize = true;
            this.rider_name_lbl.Font = new System.Drawing.Font("Century Gothic", 12.75F);
            this.rider_name_lbl.Location = new System.Drawing.Point(3, 76);
            this.rider_name_lbl.Name = "rider_name_lbl";
            this.rider_name_lbl.Size = new System.Drawing.Size(107, 20);
            this.rider_name_lbl.TabIndex = 118;
            this.rider_name_lbl.Text = "Shop Name";
            this.rider_name_lbl.Click += new System.EventHandler(this.rider_name_lbl_Click);
            // 
            // shop_id_txtbox
            // 
            this.shop_id_txtbox.Enabled = false;
            this.shop_id_txtbox.ForeColor = System.Drawing.Color.Gray;
            this.shop_id_txtbox.Location = new System.Drawing.Point(228, 31);
            this.shop_id_txtbox.Name = "shop_id_txtbox";
            this.shop_id_txtbox.Size = new System.Drawing.Size(287, 20);
            this.shop_id_txtbox.TabIndex = 115;
            this.shop_id_txtbox.Text = "ID (Autogenerated)";
            this.shop_id_txtbox.TextChanged += new System.EventHandler(this.shop_id_txtbox_TextChanged);
            this.shop_id_txtbox.Enter += new System.EventHandler(this.shop_id_txtbox_Enter);
            // 
            // rider_id_lbl
            // 
            this.rider_id_lbl.AutoSize = true;
            this.rider_id_lbl.Font = new System.Drawing.Font("Century Gothic", 12.75F);
            this.rider_id_lbl.Location = new System.Drawing.Point(3, 28);
            this.rider_id_lbl.Name = "rider_id_lbl";
            this.rider_id_lbl.Size = new System.Drawing.Size(74, 20);
            this.rider_id_lbl.TabIndex = 116;
            this.rider_id_lbl.Text = "Shop ID";
            this.rider_id_lbl.Click += new System.EventHandler(this.rider_id_lbl_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 12.75F);
            this.label5.Location = new System.Drawing.Point(603, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(275, 20);
            this.label5.TabIndex = 153;
            this.label5.Text = "Does Shopkeeper Already Exist?";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // shopkeeper_existence
            // 
            this.shopkeeper_existence.AutoSize = true;
            this.shopkeeper_existence.Location = new System.Drawing.Point(984, 31);
            this.shopkeeper_existence.Name = "shopkeeper_existence";
            this.shopkeeper_existence.Size = new System.Drawing.Size(15, 14);
            this.shopkeeper_existence.TabIndex = 154;
            this.shopkeeper_existence.UseVisualStyleBackColor = true;
            this.shopkeeper_existence.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            this.shopkeeper_existence.Click += new System.EventHandler(this.checkBox1_Click);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Black;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 19.8F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(805, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(173, 28);
            this.label4.TabIndex = 152;
            this.label4.Text = "Shopkeeper";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 12.75F);
            this.label10.Location = new System.Drawing.Point(603, 76);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(166, 20);
            this.label10.TabIndex = 155;
            this.label10.Text = "Select Shopkeeper";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // shopkeeper_combox
            // 
            this.shopkeeper_combox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.shopkeeper_combox.Enabled = false;
            this.shopkeeper_combox.ForeColor = System.Drawing.Color.Gray;
            this.shopkeeper_combox.FormattingEnabled = true;
            this.shopkeeper_combox.Location = new System.Drawing.Point(984, 79);
            this.shopkeeper_combox.Name = "shopkeeper_combox";
            this.shopkeeper_combox.Size = new System.Drawing.Size(287, 21);
            this.shopkeeper_combox.TabIndex = 156;
            this.shopkeeper_combox.SelectedIndexChanged += new System.EventHandler(this.vehicle_id_combox_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 12.75F);
            this.label9.Location = new System.Drawing.Point(603, 124);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(166, 21);
            this.label9.TabIndex = 157;
            this.label9.Text = "Shopkeeper Name";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // shopkeeper_name
            // 
            this.shopkeeper_name.ForeColor = System.Drawing.Color.Gray;
            this.shopkeeper_name.Location = new System.Drawing.Point(984, 127);
            this.shopkeeper_name.Name = "shopkeeper_name";
            this.shopkeeper_name.Size = new System.Drawing.Size(287, 20);
            this.shopkeeper_name.TabIndex = 158;
            this.shopkeeper_name.Text = "Name";
            this.shopkeeper_name.TextChanged += new System.EventHandler(this.shopkeeper_name_TextChanged);
            this.shopkeeper_name.Enter += new System.EventHandler(this.shopkeeper_name_Enter);
            // 
            // ll
            // 
            this.ll.AutoSize = true;
            this.ll.Font = new System.Drawing.Font("Century Gothic", 12.75F);
            this.ll.Location = new System.Drawing.Point(603, 173);
            this.ll.Name = "ll";
            this.ll.Size = new System.Drawing.Size(58, 21);
            this.ll.TabIndex = 159;
            this.ll.Text = "E-mail";
            this.ll.Click += new System.EventHandler(this.label8_Click);
            // 
            // shopkeeper_email
            // 
            this.shopkeeper_email.ForeColor = System.Drawing.Color.Gray;
            this.shopkeeper_email.Location = new System.Drawing.Point(984, 176);
            this.shopkeeper_email.Name = "shopkeeper_email";
            this.shopkeeper_email.Size = new System.Drawing.Size(287, 20);
            this.shopkeeper_email.TabIndex = 160;
            this.shopkeeper_email.Text = "Email";
            this.shopkeeper_email.TextChanged += new System.EventHandler(this.shopkeeper_email_TextChanged);
            this.shopkeeper_email.Enter += new System.EventHandler(this.shopkeeper_email_Enter);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.button3);
            this.panel3.Controls.Add(this.button1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(984, 293);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(456, 52);
            this.panel3.TabIndex = 163;
            this.panel3.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
            // 
            // button3
            // 
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.Black;
            this.button3.Location = new System.Drawing.Point(2, 2);
            this.button3.Margin = new System.Windows.Forms.Padding(2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(102, 38);
            this.button3.TabIndex = 150;
            this.button3.Text = "CLEAR";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Black;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(185, -2);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(102, 42);
            this.button1.TabIndex = 151;
            this.button1.Text = "ADD";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button6_Click);
            // 
            // shop_id_indicator_lblr
            // 
            this.shop_id_indicator_lblr.AutoSize = true;
            this.shop_id_indicator_lblr.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.shop_id_indicator_lblr.Location = new System.Drawing.Point(228, 48);
            this.shop_id_indicator_lblr.Name = "shop_id_indicator_lblr";
            this.shop_id_indicator_lblr.Size = new System.Drawing.Size(0, 17);
            this.shop_id_indicator_lblr.TabIndex = 164;
            this.shop_id_indicator_lblr.Click += new System.EventHandler(this.shop_id_indicator_lblr_Click);
            // 
            // shop_name_indicator_lblr
            // 
            this.shop_name_indicator_lblr.AutoSize = true;
            this.shop_name_indicator_lblr.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.shop_name_indicator_lblr.Location = new System.Drawing.Point(228, 96);
            this.shop_name_indicator_lblr.Name = "shop_name_indicator_lblr";
            this.shop_name_indicator_lblr.Size = new System.Drawing.Size(0, 17);
            this.shop_name_indicator_lblr.TabIndex = 165;
            this.shop_name_indicator_lblr.Click += new System.EventHandler(this.shop_name_indicator_lblr_Click);
            // 
            // shop_region_indicator_lblr
            // 
            this.shop_region_indicator_lblr.AutoSize = true;
            this.shop_region_indicator_lblr.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.shop_region_indicator_lblr.Location = new System.Drawing.Point(228, 147);
            this.shop_region_indicator_lblr.Name = "shop_region_indicator_lblr";
            this.shop_region_indicator_lblr.Size = new System.Drawing.Size(0, 17);
            this.shop_region_indicator_lblr.TabIndex = 166;
            this.shop_region_indicator_lblr.Click += new System.EventHandler(this.shop_region_indicator_lblr_Click);
            // 
            // shop_landline_indicator_lblr
            // 
            this.shop_landline_indicator_lblr.AutoSize = true;
            this.shop_landline_indicator_lblr.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.shop_landline_indicator_lblr.Location = new System.Drawing.Point(228, 198);
            this.shop_landline_indicator_lblr.Name = "shop_landline_indicator_lblr";
            this.shop_landline_indicator_lblr.Size = new System.Drawing.Size(0, 17);
            this.shop_landline_indicator_lblr.TabIndex = 167;
            this.shop_landline_indicator_lblr.Click += new System.EventHandler(this.shop_landline_indicator_lblr_Click);
            // 
            // shop_address_indicator_lblr
            // 
            this.shop_address_indicator_lblr.AutoSize = true;
            this.shop_address_indicator_lblr.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.shop_address_indicator_lblr.Location = new System.Drawing.Point(228, 290);
            this.shop_address_indicator_lblr.Name = "shop_address_indicator_lblr";
            this.shop_address_indicator_lblr.Size = new System.Drawing.Size(0, 17);
            this.shop_address_indicator_lblr.TabIndex = 168;
            this.shop_address_indicator_lblr.Click += new System.EventHandler(this.shop_address_indicator_lblr_Click);
            // 
            // shopkeeper_name_indicator_lbl
            // 
            this.shopkeeper_name_indicator_lbl.AutoSize = true;
            this.shopkeeper_name_indicator_lbl.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.shopkeeper_name_indicator_lbl.Location = new System.Drawing.Point(984, 147);
            this.shopkeeper_name_indicator_lbl.Name = "shopkeeper_name_indicator_lbl";
            this.shopkeeper_name_indicator_lbl.Size = new System.Drawing.Size(0, 16);
            this.shopkeeper_name_indicator_lbl.TabIndex = 169;
            this.shopkeeper_name_indicator_lbl.Click += new System.EventHandler(this.label2_Click);
            // 
            // shopkeeper_email_indicator_lbl
            // 
            this.shopkeeper_email_indicator_lbl.AutoSize = true;
            this.shopkeeper_email_indicator_lbl.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.shopkeeper_email_indicator_lbl.Location = new System.Drawing.Point(984, 198);
            this.shopkeeper_email_indicator_lbl.Name = "shopkeeper_email_indicator_lbl";
            this.shopkeeper_email_indicator_lbl.Size = new System.Drawing.Size(0, 16);
            this.shopkeeper_email_indicator_lbl.TabIndex = 170;
            // 
            // shopkeeper_phone
            // 
            this.shopkeeper_phone.ForeColor = System.Drawing.Color.Gray;
            this.shopkeeper_phone.Location = new System.Drawing.Point(984, 222);
            this.shopkeeper_phone.Name = "shopkeeper_phone";
            this.shopkeeper_phone.Size = new System.Drawing.Size(287, 20);
            this.shopkeeper_phone.TabIndex = 162;
            this.shopkeeper_phone.Text = "Phone #";
            this.shopkeeper_phone.TextChanged += new System.EventHandler(this.shop_phone_TextChanged);
            this.shopkeeper_phone.Enter += new System.EventHandler(this.shop_phone_Enter);
            // 
            // oo
            // 
            this.oo.AutoSize = true;
            this.oo.Font = new System.Drawing.Font("Century Gothic", 12.75F);
            this.oo.Location = new System.Drawing.Point(603, 219);
            this.oo.Name = "oo";
            this.oo.Size = new System.Drawing.Size(79, 21);
            this.oo.TabIndex = 161;
            this.oo.Text = "Phone #";
            this.oo.Click += new System.EventHandler(this.label1_Click);
            // 
            // shopkeeper_phone_indicator_lbl
            // 
            this.shopkeeper_phone_indicator_lbl.AutoSize = true;
            this.shopkeeper_phone_indicator_lbl.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.shopkeeper_phone_indicator_lbl.Location = new System.Drawing.Point(984, 249);
            this.shopkeeper_phone_indicator_lbl.Name = "shopkeeper_phone_indicator_lbl";
            this.shopkeeper_phone_indicator_lbl.Size = new System.Drawing.Size(0, 16);
            this.shopkeeper_phone_indicator_lbl.TabIndex = 171;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 851F));
            this.tableLayoutPanel3.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.panel2, 1, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 357);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1443, 47);
            this.tableLayoutPanel3.TabIndex = 1;
            this.tableLayoutPanel3.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel3_Paint);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button8);
            this.panel1.Controls.Add(this.textBox4);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(586, 41);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.Black;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.ForeColor = System.Drawing.Color.White;
            this.button8.Location = new System.Drawing.Point(293, 1);
            this.button8.Margin = new System.Windows.Forms.Padding(2);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(221, 38);
            this.button8.TabIndex = 128;
            this.button8.Text = "Search";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // textBox4
            // 
            this.textBox4.ForeColor = System.Drawing.Color.Gray;
            this.textBox4.Location = new System.Drawing.Point(3, 3);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(285, 20);
            this.textBox4.TabIndex = 127;
            this.textBox4.Text = "SEARCH";
            this.textBox4.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            this.textBox4.Enter += new System.EventHandler(this.textBox4_Enter);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button5);
            this.panel2.Controls.Add(this.button4);
            this.panel2.Controls.Add(this.button2);
            this.panel2.Controls.Add(this.btnClear);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(595, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(845, 41);
            this.panel2.TabIndex = 1;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Black;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.Color.White;
            this.button5.Location = new System.Drawing.Point(423, 2);
            this.button5.Margin = new System.Windows.Forms.Padding(2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(102, 37);
            this.button5.TabIndex = 149;
            this.button5.Text = "SAVE";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Black;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.Location = new System.Drawing.Point(529, 2);
            this.button4.Margin = new System.Windows.Forms.Padding(2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(102, 37);
            this.button4.TabIndex = 148;
            this.button4.Text = "EDIT";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Black;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(635, 2);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(102, 37);
            this.button2.TabIndex = 147;
            this.button2.Text = "DELETE";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.Black;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.White;
            this.btnClear.Location = new System.Drawing.Point(741, 2);
            this.btnClear.Margin = new System.Windows.Forms.Padding(2);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(102, 37);
            this.btnClear.TabIndex = 146;
            this.btnClear.Text = "PRINT";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // ShopkeeperWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1449, 765);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "ShopkeeperWindow";
            this.Text = "ShopkeeperWindow";
            this.Load += new System.EventHandler(this.ShopkeeperWindow_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.shop_datagrid)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox shop_id_txtbox;
        private System.Windows.Forms.Label rider_id_lbl;
        private System.Windows.Forms.TextBox shop_name_txtbox;
        private System.Windows.Forms.Label rider_name_lbl;
        private System.Windows.Forms.ComboBox shop_region_combox;
        private System.Windows.Forms.Label vehicle_id_lbl;
        private System.Windows.Forms.TextBox shop_landline_txtbox;
        private System.Windows.Forms.Label region_lbl;
        private System.Windows.Forms.TextBox shop_address_txtbox;
        private System.Windows.Forms.Label phone_num_lbl;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox shopkeeper_existence;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox shopkeeper_combox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox shopkeeper_name;
        private System.Windows.Forms.Label ll;
        private System.Windows.Forms.TextBox shopkeeper_email;
        private System.Windows.Forms.Label oo;
        private System.Windows.Forms.TextBox shopkeeper_phone;
        private System.Windows.Forms.DataGridView shop_datagrid;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label shop_id_indicator_lblr;
        private System.Windows.Forms.Label shop_name_indicator_lblr;
        private System.Windows.Forms.Label shop_region_indicator_lblr;
        private System.Windows.Forms.Label shop_landline_indicator_lblr;
        private System.Windows.Forms.Label shop_address_indicator_lblr;
        private System.Windows.Forms.Label shopkeeper_name_indicator_lbl;
        private System.Windows.Forms.Label shopkeeper_email_indicator_lbl;
        private System.Windows.Forms.Label shopkeeper_phone_indicator_lbl;
    }
}