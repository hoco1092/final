﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Distribution_Managment_System.BL;

namespace Distribution_Managment_System.Data_Logic
{
    class Region_CRUD
    {
        static List<RegionsofDMS> allRegions = new List<RegionsofDMS>();

        public static List<RegionsofDMS> AllRegions { get => allRegions; set => allRegions = value; }
    }
}
