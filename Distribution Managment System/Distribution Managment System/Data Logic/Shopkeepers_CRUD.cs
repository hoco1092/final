﻿using Distribution_Managment_System.BL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Distribution_Managment_System.Data_Logic
{
    class Shopkeepers_CRUD
    {
        private static List<Shopkeeper> Shopkeepers = new List<Shopkeeper>();

        internal static List<Shopkeeper> Shopkeepers1 { get => Shopkeepers; set => Shopkeepers = value; }

        public static void Add(Shopkeeper shopkeeper)
        {
            Shopkeepers1.Add(shopkeeper);
        }

        public static Shopkeeper Retrieve(String str)
        {
            foreach (Shopkeeper shopkeeper in Shopkeepers1.ToList())
            {
                if (str == shopkeeper.ShopkeeperID1)
                {
                    return shopkeeper;
                }
            }
            return null;
        }

        public static bool Delete(String str)
        {
            foreach (Shopkeeper shopkeeper in Shopkeepers1.ToList())
            {
                if (str == shopkeeper.ShopkeeperID1)
                {
                    Shopkeepers1.Remove(shopkeeper);
                    return true;
                }
            }
            return false;
        }

        public static void Update(Shopkeeper oldshopkeeper, Shopkeeper newshopkeeper)
        {
            foreach (Shopkeeper shopkeeper in Shopkeepers1.ToList())
            {
                if (shopkeeper == oldshopkeeper)
                {
                    //update

                }
            }
        }
        public static void Load()
        {
            try
            {
                string filePath = "Shopkeepers.csv";
                StreamReader reader = null;
                if (File.Exists(filePath))
                {
                    reader = new StreamReader(File.OpenRead(filePath));
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        var values = line.Split(',');
                        //  Shopkeepers.Add(new Shopkeeper(values[0], values[1], values[2], values[3], values[4]));

                    }
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Data not loaded into list", ex.Message);
            }
        }


        /*   public static List<string> GetAllShops()
           {
               List<String> Shops = new List<String>();
               foreach (Shopkeeper shopkeeper in Shopkeepers)
               {
                   foreach (Shop shop in shopkeeper.ShopsOwned1)
                   {
                       Shops.Add(shop.ShopName1 + ", " + shop.Address1 + ", " + shop.ShopID1);
                   }
               }

               return Shops;
           }*/
    }
}
