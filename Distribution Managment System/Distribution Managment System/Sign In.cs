﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Distribution_Managment_System.Data_Logic;
using System.Text.RegularExpressions;
using Distribution_Managment_System.BL;


namespace Distribution_Managment_System
{
    public partial class Sign_In : Form
    {
        public Sign_In()
        {
            InitializeComponent();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void Sign_In_Load(object sender, EventArgs e)
        {
        }
        private void btnClear_Click(object sender, EventArgs e)
        {
            txtPassword.Clear();
            txtUserName.Clear();
            lblUserNameErr.Visible = false;
            lblPasswordErr.Visible = false;
        }

        private void btnEye_MouseEnter(object sender, EventArgs e)
        {
            if (txtPassword.PasswordChar == 'O')
            {
                txtPassword.PasswordChar = '\0';
            }
        }
        private void txtUserName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //VerifyUser();
            }
        }
        private void btnEye_MouseLeave(object sender, EventArgs e)
        {
            if (txtPassword.PasswordChar == '\0')
            {
                txtPassword.PasswordChar = 'O';
            }
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            var user = UserDL.LogIn(txtUserName.Text, txtPassword.Text);
            if(user!=null){
                if(user.Role=="Admin")
                    {
                    Admin adm=(Admin) user;
                    Admin_DashBoard adminWindow = new Admin_DashBoard(adm);
                    this.Hide();
                    adminWindow.ShowDialog();
                    this.btnClear_Click(sender, e);
                    this.Show();
                }
                else if (user.Role == "Inventory Supervisor")
                {
                    Inventory_Supervisor Invsup = (Inventory_Supervisor)user;
                    ISWindow adminWindow = new ISWindow(Invsup);
                    this.Hide();
                    adminWindow.ShowDialog();
                    this.btnClear_Click(sender, e);
                    this.Show();
                }
                else if(user.Role == "Transport Manager")
                {
                    Transport_Manager TM = (Transport_Manager)user;
                    TransportManagerWindow adminWindow = new TransportManagerWindow(TM);
                    this.Hide();
                    adminWindow.ShowDialog();
                    this.btnClear_Click(sender, e);
                    this.Show();
                }
                else if (user.Role == "Sale Representative")
                {
                    Sale_Representative SR = (Sale_Representative)user;
                    SalesRepresentativeWindow adminWindow = new SalesRepresentativeWindow(SR);
                    this.Hide();
                    adminWindow.ShowDialog();
                    this.btnClear_Click(sender, e);
                    this.Show();
                }
                else if (user.Role == "Order Dispatcher")
                {
                    Order_Dispatcher OD = (Order_Dispatcher) user;
                    OrderDispatcherWindow adminWindow = new OrderDispatcherWindow(OD);
                    this.Hide();
                    adminWindow.ShowDialog();
                    this.btnClear_Click(sender, e);
                    this.Show();
                }
            }
            else
            {
                bool flag = false;
                //MessageBox.Show("Incorrect Credentials");
                if(string.IsNullOrWhiteSpace(txtUserName.Text))
                {
                    lblUserNameErr.Visible = true;
                    lblUserNameErr.Text = "User Name is Empty";
                }
                else if (txtUserName.Text.Length!=6)
                {
                    lblUserNameErr.Visible = true;
                    lblUserNameErr.Text = "User Name is of Incorrect Format (AAXXXX) ";
                }
                else
                {
                    MessageBox.Show("Incorrect Credentials");
                    flag = true;
                }
                if (string.IsNullOrWhiteSpace(txtPassword.Text))
                {
                    lblPasswordErr.Visible = true;
                    lblPasswordErr.Text = "Password is Empty";
                }
                else if(! (txtPassword.Text.Length >= 8 && txtPassword.Text.Length <= 15))
                {
                    lblPasswordErr.Visible = true;
                    lblPasswordErr.Text = "Password is of Incorrect Format (Alphanumeric 8-15) ";
                }
                else
                {
                    if(!(flag))
                        MessageBox.Show("Incorrect Credentials");
                }

            }
        }

        private void txtUserName_MouseClick(object sender, MouseEventArgs e)
        {
            lblUserNameErr.Visible = false;
        }

        private void txtPassword_MouseClick(object sender, MouseEventArgs e)
        {
            lblPasswordErr.Visible = false;
        }

        private void txtUserName_TextChanged(object sender, EventArgs e)
        {
            lblUserNameErr.Visible = false;
            txtUserName.SelectionStart = txtUserName.Text.Length;
            txtUserName.Text = txtUserName.Text.ToUpper();
        }

        private void txtPassword_TextChanged(object sender, EventArgs e)
        {
           // lblPasswordErr.Visible = false;

        }

        private void Sign_In_FormClosing(object sender, FormClosingEventArgs e)
        {
                Application.Exit();
        }

        private void txtPassword_TextChanged_1(object sender, EventArgs e)
        {

        }
    }
}
