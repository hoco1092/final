﻿using DevExpress.CodeParser;
using Distribution_Managment_System.BL;
using Distribution_Managment_System.Data_Logic;
using PresentationControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Distribution_Managment_System
{
    public partial class AddStock : Form
    {
        Inventory_Supervisor Actor;
        Product record;
        public AddStock(Inventory_Supervisor Actor)
        {
            InitializeComponent();
            this.Actor = Actor;
        }

        private void AddStock_Load(object sender, EventArgs e)
        {
            txtID.Enabled = false;
            txtName.Enabled = false;
            numUDCQty.Enabled = false;
            cmbIncDec.Items.Add("ADD STOCK");
            cmbIncDec.Items.Add("REMOVE STOCK");
            BindDataWithSuplliersComboBox();
            DataBind();
            GridColor();
        }
        private void BindDataWithSuplliersComboBox()
        {
            foreach (Supplier sup in Supplier_CRUD.Suppliers)
            {
                cmbSupplier.Items.Add(sup.Name);

            }
        }
        public void GridColor()
        {
            for (int i = 0; i < gvProducts.Columns.Count; i++)
            {
                gvProducts.Columns[i].HeaderCell.Style.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Bold);
                gvProducts.Columns[i].HeaderCell.Style.ForeColor = Color.Black;
                //gvBook.Columns[i].HeaderCell.Style.ForeColor = Color.Black;
                //gvBook.Rows[i].HeaderCell.Style.BackColor = Color.Black;
                gvProducts.Columns[i].DefaultCellStyle.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular);
                if (i % 2 == 0)
                {
                    gvProducts.Columns[i].DefaultCellStyle.BackColor = Color.FromArgb(240, 240, 240);
                }
                else
                {
                    gvProducts.Columns[i].DefaultCellStyle.BackColor = Color.Gainsboro;
                }
            }
        }
        public void DataBind()
        {
            gvProducts.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.EnableResizing;
            gvProducts.RowHeadersVisible = true;
            gvProducts.DataSource = null;
            gvProducts.Rows.Clear();
            gvProducts.DataSource = Products_CRUD.AllProducts();
            // gvProducts.DataSource = this._controller.OrderActionData;
            gvProducts.Refresh();
        }

        private void gvProducts_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
                return;

            record = (Product)gvProducts.CurrentRow.DataBoundItem;
            if ( e.ColumnIndex==0)
            {
                Erase();
                txtID.Text = record.ProductID;
                txtName.Text = record.ProductName;
                numUDCQty.Value = record.Quantity;
            }
        }

        private void cmbSupplier_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            if(cmbIncDec.SelectedIndex==0)
                lblNewQty.Text = (numUDCQty.Value + numUDQty.Value).ToString();
            else
                lblNewQty.Text = (numUDCQty.Value - numUDQty.Value).ToString();

        }

        private void btnSubmitt_Click(object sender, EventArgs e)
        {
            string Error = CheckEmpty();
            if (Error=="")
            { 
                DialogResult save = MessageBox.Show("Do you want to update record?", "CONFIRM UPDATION", MessageBoxButtons.YesNo);
                if (save == DialogResult.Yes)
                {
                    if (cmbIncDec.SelectedIndex == 0)
                        this.record.addQty((int)numUDQty.Value);
                    else
                        this.record.deleteQty((int)numUDQty.Value);
                    Actor.UpdateProduct(record);
                    foreach (CheckBoxComboBoxItem item in cmbSupplier.CheckBoxItems)
                    {
                        if (item.Checked)
                            Supplier_CRUD.AddProducttoSupplier(item.ToString(), record);
                    }
                    Actor.UpdateSupplier();
                    btnSubmitt.Text = "SAVE";
                    btnClear.Text = "CLEAR";
                    Erase();
                    DataBind();
                    GridColor();
                }
            }
            else
            {
                MessageBox.Show(Error);
            }
        }
        private string CheckEmpty()
        {
            string error = "";
            if (txtID.Text == "")
            {
                error += "Select Item To Add Stock\n";


             if (numUDCQty.Value < 0)
                {
                    error += "Adding Stock requires quantity to be greated than zero\n";
                }
                 if (cmbIncDec.SelectedIndex == 1 && 2 * numUDCQty.Value - record.Quantity < 0)
                {
                    error += "Quantity must be greated than zero\n";
                }
                if (cmbIncDec.SelectedIndex == -1)
                {
                    error += "Selcted Incremet or Decrement Stock from Combo Box\n";
                }
                bool flag = true;
                foreach (CheckBoxComboBoxItem item in cmbSupplier.CheckBoxItems)
                {
                    if (item.Checked)
                    {
                        flag = false;
                        break;

                    }

                }
                if (flag)
                {
                    error += "Selcted atleast one supplier\n";
                }
            }
            return error;
        }
        public void Erase()
        {
            txtID.Text = "";
            txtName.Text = "";
            numUDCQty.ResetText() ;
            numUDQty.ResetText();
            lblNewQty.Text="";
            cmbIncDec.SelectedItem = null;
            cmbSupplier.ClearSelection();
        }
        private void btnClear_Click(object sender, EventArgs e)
        {
            Erase();
        }

        private void AddStock_Resize(object sender, EventArgs e)
        {
            float newSize = 12.2f;
            if (this.Width < 825)
            {
                if (this.Width < 615)
                {
                    newSize = 10.2f;
                }
            }
            else
            {
                newSize = 14f;
            }
            lblProductID.Font = new Font(lblProduct.Font.FontFamily, newSize, lblProduct.Font.Style);
            lblProduct.Font = new Font(lblProduct.Font.FontFamily, newSize, lblProduct.Font.Style);
            lblIncDec.Font = new Font(lblProduct.Font.FontFamily, newSize, lblProduct.Font.Style);
            lblCurrentQty.Font = new Font(lblProduct.Font.FontFamily, newSize, lblProduct.Font.Style);
            lblNewQuantity.Font = new Font(lblProduct.Font.FontFamily, newSize, lblProduct.Font.Style);
            lblQty.Font = new Font(lblProduct.Font.FontFamily, newSize, lblProduct.Font.Style);
            lblSupplier.Font = new Font(lblProduct.Font.FontFamily, newSize, lblProduct.Font.Style);
            txtID.Font = new Font(lblProduct.Font.FontFamily, newSize, lblProduct.Font.Style);
            txtName.Font = new Font(lblProduct.Font.FontFamily, newSize, lblProduct.Font.Style);
            numUDCQty.Font = new Font(lblProduct.Font.FontFamily, newSize, lblProduct.Font.Style);
            numUDQty.Font = new Font(lblProduct.Font.FontFamily, newSize, lblProduct.Font.Style);
            cmbSupplier.Font = new Font(lblProduct.Font.FontFamily, newSize, lblProduct.Font.Style);
            cmbIncDec.Font = new Font(lblProduct.Font.FontFamily, newSize, lblProduct.Font.Style);
            btnClear.Font = new Font(btnClear.Font.FontFamily, newSize, btnClear.Font.Style);
            btnSubmitt.Font = new Font(btnClear.Font.FontFamily, newSize, btnClear.Font.Style);
        }

        private void cmbIncDec_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbIncDec.SelectedIndex == 0)
                lblNewQty.Text = (numUDCQty.Value + numUDQty.Value).ToString();
            else
                lblNewQty.Text = (numUDCQty.Value - numUDQty.Value).ToString();
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (txtSearch.Text != string.Empty)
            {

                foreach (DataGridViewRow row in gvProducts.Rows)
                {
                    string text = "";
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        if (cell.ColumnIndex != 0 )
                        {
                            text += cell.Value.ToString();
                        }

                    }
                    if (text.ToUpper().Contains(txtSearch.Text.ToUpper()))
                    {
                        CurrencyManager currencyManager1 = (CurrencyManager)BindingContext[gvProducts.DataSource];
                        currencyManager1.SuspendBinding();
                        row.Visible = true;
                        currencyManager1.ResumeBinding();
                    }
                    else
                    {
                        CurrencyManager currencyManager1 = (CurrencyManager)BindingContext[gvProducts.DataSource];
                        currencyManager1.SuspendBinding();
                        row.Visible = false;
                        currencyManager1.ResumeBinding();
                    }
                }
            }
            else
            {
                foreach (DataGridViewRow row in gvProducts.Rows)
                {
                    CurrencyManager currencyManager1 = (CurrencyManager)BindingContext[gvProducts.DataSource];
                    currencyManager1.SuspendBinding();
                    row.Visible = true;
                    currencyManager1.ResumeBinding();
                }
            }
        }
    }
}
