﻿namespace Distribution_Managment_System
{
    partial class SalesRepresentativeWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SalesRepresentativeWindow));
            this.pnlMenu = new System.Windows.Forms.Panel();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnOrderHostory = new System.Windows.Forms.Button();
            this.btnOrderDashboard = new System.Windows.Forms.Button();
            this.btnShopKeeperDashboard = new System.Windows.Forms.Button();
            this.pnlLogo = new System.Windows.Forms.Panel();
            this.lblName = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblTtleBar = new System.Windows.Forms.Label();
            this.pnlTitleBar = new System.Windows.Forms.Panel();
            this.pnlWindow = new System.Windows.Forms.Panel();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.pnlMenu.SuspendLayout();
            this.pnlLogo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlTitleBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMenu
            // 
            this.pnlMenu.AutoScroll = true;
            this.pnlMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(1)))), ((int)(((byte)(51)))));
            this.pnlMenu.Controls.Add(this.btnExit);
            this.pnlMenu.Controls.Add(this.btnOrderHostory);
            this.pnlMenu.Controls.Add(this.btnOrderDashboard);
            this.pnlMenu.Controls.Add(this.btnShopKeeperDashboard);
            this.pnlMenu.Controls.Add(this.pnlLogo);
            this.pnlMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlMenu.ForeColor = System.Drawing.Color.Gainsboro;
            this.pnlMenu.Location = new System.Drawing.Point(0, 0);
            this.pnlMenu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlMenu.Name = "pnlMenu";
            this.pnlMenu.Size = new System.Drawing.Size(251, 703);
            this.pnlMenu.TabIndex = 4;
            // 
            // btnExit
            // 
            this.btnExit.AutoSize = true;
            this.btnExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(1)))), ((int)(((byte)(51)))));
            this.btnExit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnExit.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Image")));
            this.btnExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.Location = new System.Drawing.Point(0, 352);
            this.btnExit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnExit.Name = "btnExit";
            this.btnExit.Padding = new System.Windows.Forms.Padding(11, 0, 0, 0);
            this.btnExit.Size = new System.Drawing.Size(251, 69);
            this.btnExit.TabIndex = 10;
            this.btnExit.Text = "  EXIT";
            this.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnOrderHostory
            // 
            this.btnOrderHostory.AutoSize = true;
            this.btnOrderHostory.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(1)))), ((int)(((byte)(51)))));
            this.btnOrderHostory.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnOrderHostory.FlatAppearance.BorderSize = 0;
            this.btnOrderHostory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOrderHostory.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOrderHostory.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnOrderHostory.Image = ((System.Drawing.Image)(resources.GetObject("btnOrderHostory.Image")));
            this.btnOrderHostory.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOrderHostory.Location = new System.Drawing.Point(0, 283);
            this.btnOrderHostory.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnOrderHostory.Name = "btnOrderHostory";
            this.btnOrderHostory.Padding = new System.Windows.Forms.Padding(11, 0, 0, 0);
            this.btnOrderHostory.Size = new System.Drawing.Size(251, 69);
            this.btnOrderHostory.TabIndex = 7;
            this.btnOrderHostory.Text = "ORDER HISTORY";
            this.btnOrderHostory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOrderHostory.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnOrderHostory.UseVisualStyleBackColor = false;
            this.btnOrderHostory.Click += new System.EventHandler(this.btnOrderHostory_Click);
            // 
            // btnOrderDashboard
            // 
            this.btnOrderDashboard.AutoSize = true;
            this.btnOrderDashboard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(1)))), ((int)(((byte)(51)))));
            this.btnOrderDashboard.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnOrderDashboard.FlatAppearance.BorderSize = 0;
            this.btnOrderDashboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOrderDashboard.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOrderDashboard.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnOrderDashboard.Image = ((System.Drawing.Image)(resources.GetObject("btnOrderDashboard.Image")));
            this.btnOrderDashboard.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOrderDashboard.Location = new System.Drawing.Point(0, 223);
            this.btnOrderDashboard.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnOrderDashboard.Name = "btnOrderDashboard";
            this.btnOrderDashboard.Padding = new System.Windows.Forms.Padding(11, 0, 0, 0);
            this.btnOrderDashboard.Size = new System.Drawing.Size(251, 60);
            this.btnOrderDashboard.TabIndex = 4;
            this.btnOrderDashboard.Text = "ORDER DASHBOARD";
            this.btnOrderDashboard.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOrderDashboard.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnOrderDashboard.UseVisualStyleBackColor = false;
            this.btnOrderDashboard.Click += new System.EventHandler(this.btnOrderDashboard_Click);
            // 
            // btnShopKeeperDashboard
            // 
            this.btnShopKeeperDashboard.AutoSize = true;
            this.btnShopKeeperDashboard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(1)))), ((int)(((byte)(51)))));
            this.btnShopKeeperDashboard.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnShopKeeperDashboard.FlatAppearance.BorderSize = 0;
            this.btnShopKeeperDashboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnShopKeeperDashboard.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnShopKeeperDashboard.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnShopKeeperDashboard.Image = global::Distribution_Managment_System.Properties.Resources.products__2_;
            this.btnShopKeeperDashboard.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnShopKeeperDashboard.Location = new System.Drawing.Point(0, 154);
            this.btnShopKeeperDashboard.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnShopKeeperDashboard.Name = "btnShopKeeperDashboard";
            this.btnShopKeeperDashboard.Padding = new System.Windows.Forms.Padding(11, 0, 0, 0);
            this.btnShopKeeperDashboard.Size = new System.Drawing.Size(251, 69);
            this.btnShopKeeperDashboard.TabIndex = 0;
            this.btnShopKeeperDashboard.Text = "SHOPKEEPER DASHBOARD";
            this.btnShopKeeperDashboard.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnShopKeeperDashboard.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnShopKeeperDashboard.UseVisualStyleBackColor = false;
            this.btnShopKeeperDashboard.Click += new System.EventHandler(this.btnShopKeeperDashboard_Click);
            // 
            // pnlLogo
            // 
            this.pnlLogo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(39)))), ((int)(((byte)(58)))));
            this.pnlLogo.Controls.Add(this.lblName);
            this.pnlLogo.Controls.Add(this.pictureBox1);
            this.pnlLogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlLogo.ForeColor = System.Drawing.Color.Gainsboro;
            this.pnlLogo.Location = new System.Drawing.Point(0, 0);
            this.pnlLogo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlLogo.Name = "pnlLogo";
            this.pnlLogo.Size = new System.Drawing.Size(251, 154);
            this.pnlLogo.TabIndex = 0;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.BackColor = System.Drawing.Color.Transparent;
            this.lblName.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblName.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.ForeColor = System.Drawing.Color.White;
            this.lblName.Location = new System.Drawing.Point(0, 133);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(63, 21);
            this.lblName.TabIndex = 5;
            this.lblName.Text = "NAME";
            this.lblName.Click += new System.EventHandler(this.lblName_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = global::Distribution_Managment_System.Properties.Resources.DMS_Logo;
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(251, 154);
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // lblTtleBar
            // 
            this.lblTtleBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTtleBar.Font = new System.Drawing.Font("Century Gothic", 48F, System.Drawing.FontStyle.Bold);
            this.lblTtleBar.ForeColor = System.Drawing.Color.White;
            this.lblTtleBar.Location = new System.Drawing.Point(0, 0);
            this.lblTtleBar.Name = "lblTtleBar";
            this.lblTtleBar.Size = new System.Drawing.Size(1227, 100);
            this.lblTtleBar.TabIndex = 0;
            this.lblTtleBar.Text = "Title";
            this.lblTtleBar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlTitleBar
            // 
            this.pnlTitleBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(28)))), ((int)(((byte)(70)))));
            this.pnlTitleBar.Controls.Add(this.lblTtleBar);
            this.pnlTitleBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTitleBar.Location = new System.Drawing.Point(251, 0);
            this.pnlTitleBar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlTitleBar.Name = "pnlTitleBar";
            this.pnlTitleBar.Size = new System.Drawing.Size(1227, 100);
            this.pnlTitleBar.TabIndex = 5;
            // 
            // pnlWindow
            // 
            this.pnlWindow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlWindow.Location = new System.Drawing.Point(251, 100);
            this.pnlWindow.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlWindow.Name = "pnlWindow";
            this.pnlWindow.Size = new System.Drawing.Size(1227, 603);
            this.pnlWindow.TabIndex = 6;
            // 
            // SalesRepresentative
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1478, 703);
            this.Controls.Add(this.pnlWindow);
            this.Controls.Add(this.pnlTitleBar);
            this.Controls.Add(this.pnlMenu);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MinimumSize = new System.Drawing.Size(730, 650);
            this.Name = "SalesRepresentative";
            this.Text = "MainWinow";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainWinow_Load);
            this.Resize += new System.EventHandler(this.MainWindow_Resize);
            this.pnlMenu.ResumeLayout(false);
            this.pnlMenu.PerformLayout();
            this.pnlLogo.ResumeLayout(false);
            this.pnlLogo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlTitleBar.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel pnlMenu;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnOrderHostory;
        private System.Windows.Forms.Button btnOrderDashboard;
        private System.Windows.Forms.Panel pnlLogo;
        private System.Windows.Forms.Label lblTtleBar;
        private System.Windows.Forms.Panel pnlTitleBar;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Panel pnlWindow;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        public System.Windows.Forms.Button btnShopKeeperDashboard;
    }
}