﻿namespace Distribution_Managment_System
{
    partial class Generate_Report
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.tbLayControls = new System.Windows.Forms.TableLayoutPanel();
            this.lblEndTo = new System.Windows.Forms.Label();
            this.lblStartFrom = new System.Windows.Forms.Label();
            this.lblReport = new System.Windows.Forms.Label();
            this.cmbReport = new System.Windows.Forms.ComboBox();
            this.pnlButtons = new System.Windows.Forms.Panel();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnSubmitt = new System.Windows.Forms.Button();
            this.datPStart = new System.Windows.Forms.DateTimePicker();
            this.datPEnd = new System.Windows.Forms.DateTimePicker();
            this.pdfViewer1 = new DevExpress.XtraPdfViewer.PdfViewer();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tbLayControls.SuspendLayout();
            this.pnlButtons.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.pdfViewer1, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(925, 817);
            this.tableLayoutPanel1.TabIndex = 1;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tbLayControls, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 85.71429F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(919, 320);
            this.tableLayoutPanel2.TabIndex = 52;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label6);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(913, 39);
            this.panel1.TabIndex = 120;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Black;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 19.8F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(913, 39);
            this.label6.TabIndex = 111;
            this.label6.Text = "GENERATE REPORT";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbLayControls
            // 
            this.tbLayControls.ColumnCount = 2;
            this.tbLayControls.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 43.73994F));
            this.tbLayControls.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 56.26006F));
            this.tbLayControls.Controls.Add(this.lblEndTo, 0, 2);
            this.tbLayControls.Controls.Add(this.lblStartFrom, 0, 1);
            this.tbLayControls.Controls.Add(this.lblReport, 0, 0);
            this.tbLayControls.Controls.Add(this.cmbReport, 1, 0);
            this.tbLayControls.Controls.Add(this.pnlButtons, 1, 3);
            this.tbLayControls.Controls.Add(this.datPStart, 1, 1);
            this.tbLayControls.Controls.Add(this.datPEnd, 1, 2);
            this.tbLayControls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbLayControls.Location = new System.Drawing.Point(3, 48);
            this.tbLayControls.Name = "tbLayControls";
            this.tbLayControls.RowCount = 4;
            this.tbLayControls.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tbLayControls.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tbLayControls.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tbLayControls.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tbLayControls.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tbLayControls.Size = new System.Drawing.Size(913, 269);
            this.tbLayControls.TabIndex = 0;
            // 
            // lblEndTo
            // 
            this.lblEndTo.AutoSize = true;
            this.lblEndTo.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblEndTo.Font = new System.Drawing.Font("Century Gothic", 16.2F);
            this.lblEndTo.Location = new System.Drawing.Point(4, 134);
            this.lblEndTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEndTo.Name = "lblEndTo";
            this.lblEndTo.Size = new System.Drawing.Size(44, 67);
            this.lblEndTo.TabIndex = 117;
            this.lblEndTo.Text = "To";
            this.lblEndTo.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblStartFrom
            // 
            this.lblStartFrom.AutoSize = true;
            this.lblStartFrom.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblStartFrom.Font = new System.Drawing.Font("Century Gothic", 16.2F);
            this.lblStartFrom.Location = new System.Drawing.Point(4, 67);
            this.lblStartFrom.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStartFrom.Name = "lblStartFrom";
            this.lblStartFrom.Size = new System.Drawing.Size(81, 67);
            this.lblStartFrom.TabIndex = 118;
            this.lblStartFrom.Text = "From";
            this.lblStartFrom.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblReport
            // 
            this.lblReport.AutoSize = true;
            this.lblReport.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblReport.Font = new System.Drawing.Font("Century Gothic", 16.2F);
            this.lblReport.Location = new System.Drawing.Point(4, 0);
            this.lblReport.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblReport.Name = "lblReport";
            this.lblReport.Size = new System.Drawing.Size(197, 67);
            this.lblReport.TabIndex = 116;
            this.lblReport.Text = "Select Report";
            this.lblReport.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cmbReport
            // 
            this.cmbReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbReport.Font = new System.Drawing.Font("Century Gothic", 13.8F);
            this.cmbReport.FormattingEnabled = true;
            this.cmbReport.Location = new System.Drawing.Point(402, 3);
            this.cmbReport.MaximumSize = new System.Drawing.Size(570, 0);
            this.cmbReport.MinimumSize = new System.Drawing.Size(250, 0);
            this.cmbReport.Name = "cmbReport";
            this.cmbReport.Size = new System.Drawing.Size(508, 35);
            this.cmbReport.TabIndex = 120;
            // 
            // pnlButtons
            // 
            this.pnlButtons.Controls.Add(this.btnClear);
            this.pnlButtons.Controls.Add(this.btnSubmitt);
            this.pnlButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlButtons.Location = new System.Drawing.Point(402, 204);
            this.pnlButtons.MaximumSize = new System.Drawing.Size(570, 45);
            this.pnlButtons.MinimumSize = new System.Drawing.Size(250, 0);
            this.pnlButtons.Name = "pnlButtons";
            this.pnlButtons.Size = new System.Drawing.Size(508, 45);
            this.pnlButtons.TabIndex = 121;
            // 
            // btnClear
            // 
            this.btnClear.AutoSize = true;
            this.btnClear.BackColor = System.Drawing.Color.White;
            this.btnClear.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(8)))), ((int)(((byte)(81)))));
            this.btnClear.Location = new System.Drawing.Point(0, 0);
            this.btnClear.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnClear.MaximumSize = new System.Drawing.Size(0, 40);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(94, 40);
            this.btnClear.TabIndex = 3;
            this.btnClear.Text = "CLEAR";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnSubmitt
            // 
            this.btnSubmitt.AutoSize = true;
            this.btnSubmitt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(1)))), ((int)(((byte)(51)))));
            this.btnSubmitt.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSubmitt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSubmitt.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubmitt.ForeColor = System.Drawing.Color.White;
            this.btnSubmitt.Location = new System.Drawing.Point(372, 0);
            this.btnSubmitt.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSubmitt.MaximumSize = new System.Drawing.Size(0, 40);
            this.btnSubmitt.Name = "btnSubmitt";
            this.btnSubmitt.Size = new System.Drawing.Size(136, 40);
            this.btnSubmitt.TabIndex = 1;
            this.btnSubmitt.Text = "GENERATE";
            this.btnSubmitt.UseVisualStyleBackColor = false;
            this.btnSubmitt.Click += new System.EventHandler(this.btnSubmitt_Click);
            // 
            // datPStart
            // 
            this.datPStart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.datPStart.Font = new System.Drawing.Font("Century Gothic", 13.8F);
            this.datPStart.Location = new System.Drawing.Point(402, 70);
            this.datPStart.MaximumSize = new System.Drawing.Size(570, 36);
            this.datPStart.MinimumSize = new System.Drawing.Size(250, 4);
            this.datPStart.Name = "datPStart";
            this.datPStart.Size = new System.Drawing.Size(508, 36);
            this.datPStart.TabIndex = 122;
            // 
            // datPEnd
            // 
            this.datPEnd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.datPEnd.Font = new System.Drawing.Font("Century Gothic", 13.8F);
            this.datPEnd.Location = new System.Drawing.Point(402, 137);
            this.datPEnd.MaximumSize = new System.Drawing.Size(570, 36);
            this.datPEnd.MinimumSize = new System.Drawing.Size(250, 4);
            this.datPEnd.Name = "datPEnd";
            this.datPEnd.Size = new System.Drawing.Size(508, 36);
            this.datPEnd.TabIndex = 123;
            // 
            // pdfViewer1
            // 
            this.pdfViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pdfViewer1.Location = new System.Drawing.Point(3, 329);
            this.pdfViewer1.Name = "pdfViewer1";
            this.pdfViewer1.Size = new System.Drawing.Size(919, 485);
            this.pdfViewer1.TabIndex = 53;
            // 
            // Generate_Report
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(925, 817);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Generate_Report";
            this.Text = "Generate_Report";
            this.Load += new System.EventHandler(this.Generate_Report_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tbLayControls.ResumeLayout(false);
            this.tbLayControls.PerformLayout();
            this.pnlButtons.ResumeLayout(false);
            this.pnlButtons.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TableLayoutPanel tbLayControls;
        private System.Windows.Forms.Label lblEndTo;
        private System.Windows.Forms.Label lblStartFrom;
        private System.Windows.Forms.Label lblReport;
        private System.Windows.Forms.ComboBox cmbReport;
        private System.Windows.Forms.Panel pnlButtons;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnSubmitt;
        private System.Windows.Forms.DateTimePicker datPStart;
        private System.Windows.Forms.DateTimePicker datPEnd;
        private DevExpress.XtraPdfViewer.PdfViewer pdfViewer1;
    }
}