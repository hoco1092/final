﻿using Distribution_Managment_System.Data_Logic;
using Distribution_Managment_System.BL;
using Distribution_Managment_System.Analytical_Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iText.Forms;
using iText.Forms.Fields;
using iText.IO.Source;
using iText.Kernel.Pdf;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using iText.Layout;
using iText.Layout.Element;
using iText.Layout.Properties;
using iText.Kernel.Pdf.Canvas.Draw;
using DevExpress.XtraReports.UI;
using iText.Kernel.Font;
using iText.Layout.Font;
using iText.IO.Font;

namespace Distribution_Managment_System
{
    public partial class Generate_Report : Form
    {
        public Generate_Report()
        {
            InitializeComponent();
        }

        private void Generate_Report_Load(object sender, EventArgs e)
        {
            this.cmbReport.DataSource= Enum.GetNames(typeof(Reports));
        }

        private void btnSubmitt_Click(object sender, EventArgs e)
        {
            this.pdfViewer1.CloseDocument();
            string err = Validations();
            if (err=="")
            {
                if(cmbReport.SelectedItem.ToString() == Reports.Sales_Report.ToString()) 
                {

                        ReportsGeneration.SalesReport(this.datPStart.Value, this.datPEnd.Value);
                    /*//XtraReport1 report=new XtraReport1(this.datPStart.Value,this.datPEnd.Value,SalesReportDataCRUD.SaleReportDatas);
                    SalesReportDev report = new SalesReportDev();
                report.ShowPreviewDialog();    
                report.ExportToPdf(@"..\..\Report.pdf");
                    this.pdfViewer1.LoadDocument(@"..\..\Report.pdf");*/
                    String path = @"..\..\SalesReport.pdf";
                    PdfWriter writer = new PdfWriter(path);
                    PdfDocument pdf = new PdfDocument(writer);
                    Document document = new Document(pdf);
                    PdfFont font = PdfFontFactory.CreateFont();
                    Paragraph header = new Paragraph("SALES REPORT")
                        .SetTextAlignment(TextAlignment.CENTER)
                        .SetFont(font)
                        .SetFontSize(24)
                        .SetBold();
                    document.Add(header);
                    LineSeparator l1 = new LineSeparator(new SolidLine());
                    foreach (SaleReportData s in SalesReportDataCRUD.getData())
                    {
                        document.Add(l1);
                        Paragraph subheader = new Paragraph("Region: "+s.Region)
                           .SetTextAlignment(TextAlignment.CENTER)
                           .SetFontSize(14)
                           .SetBold();
                        document.Add(l1);
                        document.Add(subheader);
                        Paragraph Revenue = new Paragraph("Revenue: " + s.Revenue.ToString())
                           .SetTextAlignment(TextAlignment.CENTER)
                           .SetFontSize(12);
                        document.Add(Revenue);
                        Paragraph Sales = new Paragraph("Sales: "+s.Sales.ToString())
                          .SetTextAlignment(TextAlignment.CENTER)
                          .SetFontSize(12);
                        document.Add(Sales);
                        Paragraph Profit = new Paragraph("Percentage: "+s.PercentageProfit.ToString()+"%")
                          .SetTextAlignment(TextAlignment.CENTER)
                          .SetFontSize(12);
                        document.Add(Profit);

                    }
                        document.Close();
                        this.pdfViewer1.LoadDocument(@"..\..\SalesReport.pdf");

                }
                else if (cmbReport.SelectedItem.ToString() == Reports.Top_Sellers_Report.ToString())
                {

                    ReportsGeneration.TopSellerReport(this.datPStart.Value, this.datPEnd.Value);
                    /*//XtraReport1 report=new XtraReport1(this.datPStart.Value,this.datPEnd.Value,SalesReportDataCRUD.SaleReportDatas);
                    SalesReportDev report = new SalesReportDev();
                report.ShowPreviewDialog();    
                report.ExportToPdf(@"..\..\Report.pdf");
                    this.pdfViewer1.LoadDocument(@"..\..\Report.pdf");*/
                    String path = @"..\..\TopSellersReport.pdf";
                    PdfWriter writer = new PdfWriter(path);
                    PdfDocument pdf = new PdfDocument(writer);
                    Document document = new Document(pdf);
                    PdfFont font = PdfFontFactory.CreateFont();
                    Paragraph header = new Paragraph("TOP SELLER REPORT")
                        .SetTextAlignment(TextAlignment.CENTER)
                        .SetFont(font)
                        .SetFontSize(24)
                        .SetBold();
                    document.Add(header);
                    LineSeparator l1 = new LineSeparator(new SolidLine());
                    foreach (TopSeller s in TopSellerDataCRUD.getData())
                    {
                        document.Add(l1);
                        Paragraph subheader = new Paragraph(s.Region)
                           .SetTextAlignment(TextAlignment.CENTER)
                           .SetFontSize(14)
                           .SetBold();
                        document.Add(l1);
                        document.Add(subheader);
                        Paragraph Product = new Paragraph("Product: "+s.Product.ToString())
                           .SetTextAlignment(TextAlignment.CENTER)
                           .SetFontSize(12);
                        document.Add(Product);
                        Paragraph Sales = new Paragraph("Sales: "+s.Sales.ToString())
                          .SetTextAlignment(TextAlignment.CENTER)
                          .SetFontSize(12);
                        document.Add(Sales);
                        Paragraph Profit = new Paragraph("Revenue: "+s.Revenue.ToString())
                          .SetTextAlignment(TextAlignment.CENTER)
                          .SetFontSize(12);
                        document.Add(Profit);
                        Paragraph Quantity = new Paragraph("Quantity: "+s.Quantity.ToString())
                          .SetTextAlignment(TextAlignment.CENTER)
                          .SetFontSize(12);
                        document.Add(Quantity);

                    }
                    document.Close();
                    this.pdfViewer1.LoadDocument(@"..\..\TopSellersReport.pdf");
                }

            }
            else
            {
                MessageBox.Show(err);
            }
        }
        private string Validations()
        {
            string error = "";
            if (!(DateTime.Compare(this.datPStart.Value.Date, DateTime.Today.Date) <= 0))
                error += "Start Date must be less than or equal to Today's Date\n";
            if (!(DateTime.Compare(this.datPEnd.Value.Date, DateTime.Today.Date) <= 0))
                error += "End Date must be less than or equal to Today's Date\n";
            if (cmbReport.SelectedIndex<0)
                error += "First Select Report\n";
            if (!(DateTime.Compare(this.datPStart.Value.Date, this.datPEnd.Value.Date) <= 0))
                error += "End Date must be greater than or equal to Start Date\n";
            return error;
        }
        private XtraReport CreateReport(IList<SaleReportData> lst)
        {
            XtraReport report = new XtraReport();
            DetailBand detail = new DetailBand();
            detail.Height = 30;
            report.Bands.Add(detail);

            DetailReportBand detailReport1 = new DetailReportBand();
            report.Bands.Add(detailReport1);

            DetailBand detail1 = new DetailBand();
            detail1.Height = 30;
            detailReport1.Bands.Add(detail1);

            DetailReportBand detailReport2 = new DetailReportBand();
            detailReport1.Bands.Add(detailReport2);

            DetailBand detail2 = new DetailBand();
            detail2.Height = 30;
            detailReport2.Bands.Add(detail2);

            report.DataSource = lst;

            //detailReport2.DataMember = "Products.OrderDetails";

            detail.Controls.Add(CreateBoundLabel("Region", Color.Gold, 0));
            detail1.Controls.Add(CreateBoundLabel("Revenue", Color.Gold, 100));
            detail2.Controls.Add(CreateBoundLabel("Sales", Color.Gold, 200));
            //detail.Controls.Add(CreateBoundLabel("PercentageProfit", Color.Gold, 30));

            return report;
        }
        private XtraReport CreateReport(IList<TopSeller> lst)
        {
            XtraReport report = new XtraReport();
            DetailBand detail = new DetailBand();
            detail.Height = 30;
            report.Bands.Add(detail);

            DetailReportBand detailReport1 = new DetailReportBand();
            report.Bands.Add(detailReport1);

            DetailBand detail1 = new DetailBand();
            detail1.Height = 30;
            detailReport1.Bands.Add(detail1);

            DetailReportBand detailReport2 = new DetailReportBand();
            detailReport1.Bands.Add(detailReport2);

            DetailBand detail2 = new DetailBand();
            detail2.Height = 30;
            detailReport2.Bands.Add(detail2);

            report.DataSource = lst;

            //detailReport2.DataMember = "Products.OrderDetails";

            detail.Controls.Add(CreateBoundLabel("Region", Color.Gold, 0));
            detail2.Controls.Add(CreateBoundLabel("Product", Color.Gold, 200));
            detail2.Controls.Add(CreateBoundLabel("Quantity", Color.Gold, 200));
            detail1.Controls.Add(CreateBoundLabel("Revenue", Color.Gold, 100));
            detail2.Controls.Add(CreateBoundLabel("Sales", Color.Gold, 200));
            //detail.Controls.Add(CreateBoundLabel("PercentageProfit", Color.Gold, 30));

            return report;
        }

        private XRLabel CreateBoundLabel(string dataMember, Color backColor, int offset)
        {
            XRLabel label = new XRLabel();

            label.DataBindings.Add(new XRBinding("Text", null, dataMember));
            label.BackColor = backColor;
            label.Location = new Point(offset, 0);

            return label;
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            this.cmbReport.SelectedIndex = -1;
            //this.datPEnd.Value = DateTime.MinValue; 
            //this.datPStart.Value = DateTime.MinValue;
            this.pdfViewer1.CloseDocument();
        }
    }
}
