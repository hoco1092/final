﻿using Distribution_Managment_System.BL;
using Distribution_Managment_System.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Distribution_Managment_System
{
    public partial class TransportManagerWindow : Form
    {
        Transport_Manager Actor;
        private static Color colour;
        private Button currentButton;
        private Random random;
        private int tempIndex;
        private Form activeForm;
        private string nameofUser;
        public TransportManagerWindow(Transport_Manager Actor)
        {
            InitializeComponent();
            random = new Random();
            this.Actor=Actor;
        }
        private Color SelectThemeColor()
        {
            int index = random.Next(ThemeColor.ColorList.Count);
            while (tempIndex == index)
            {
                index = random.Next(ThemeColor.ColorList.Count);
            }
            tempIndex = index;
            string color = ThemeColor.ColorList[index];
            return ColorTranslator.FromHtml(color);
        }
        private void ActivateButton(object btnSender)
        {
            if (btnSender != null)
            {
                if (currentButton != (Button)btnSender)
                {
                    DisableButton();
                    Color color = SelectThemeColor();
                    currentButton = (Button)btnSender;
                    currentButton.BackColor = color;
                    currentButton.ForeColor = Color.White;
                    currentButton.Font = new System.Drawing.Font("Century Gothic", 12.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    pnlTitleBar.BackColor = color;
                    pnlLogo.BackColor = ThemeColor.ChangeColorBrightness(color, -0.3);
                    pnlLogo.BackColor = ThemeColor.ChangeColorBrightness(color, -0.3);
                    ThemeColor.PrimaryColor = color;
                    ThemeColor.SecondaryColor = ThemeColor.ChangeColorBrightness(color, -0.3);
                    colour = color;
                    //btnCloseChildForm.Visible = true;
                }
            }
        }
        private void OpenChildForm(Form childForm, object btnSender)
        {
            if (activeForm != null)
                activeForm.Close();
            ActivateButton(btnSender);
            activeForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            this.pnlWindow.Controls.Add(childForm);
            this.pnlWindow.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();
            lblTtleBar.Text = childForm.Text;
        }
        private void DisableButton()
        {
            foreach (Control previousBtn in pnlMenu.Controls)
            {
                if (previousBtn.GetType() == typeof(Button))
                {
                    previousBtn.BackColor = Color.FromArgb(8, 1, 51);
                    previousBtn.ForeColor = Color.Gainsboro;
                    previousBtn.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                }
            }
        }
        private void btnUserDashboard_Click(object sender, EventArgs e)
        {
            if(currentButton!= btnRiderDashBoard)
            {
                RiderWindow frm = new RiderWindow();
                OpenChildForm(frm, sender);
                lblTtleBar.Text = "RiderDashboard DashBoard";
            }
        }

        private void lblName_Click(object sender, EventArgs e)
        {

        }

        private void btnBook_Click(object sender, EventArgs e)
        {
            if (currentButton != sender)
            {
                VehicleWindow frm = new VehicleWindow();
                OpenChildForm(frm, sender);
                lblTtleBar.Text = "Vehicle DashBoard";
            }

        }

        private void pnlWindow_Paint(object sender, PaintEventArgs e)
        {

        }

        private void MainWinow_Load(object sender, EventArgs e)
        {
            lblName.Text=Actor.Name;
        }
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle = cp.ExStyle | 0x2000000;
                return cp;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();

        }

       

        private void MainWindow_ResizeBegin(object sender, EventArgs e)
        {

        }

        private void MainWindow_Resize(object sender, EventArgs e)
        {
        }
    }
}
