﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Distribution_Managment_System.BL;

namespace Distribution_Managment_System.Data_Structures
{
    
    public class Node<M> where M : class 
    {
           M data;
           Node<M> next;
           Node<M> prev;

           public M Data { get => data; set => data = value; }
           public Node<M> Next { get => next; set => next = value; }
           public Node<M> Prev { get => prev; set => prev = value; }

           public Node(M data)
           {
               Data = data;
               next = null;
               prev = null;
           }

           public M getData()
           {
               return Data;
           }

           public void setData(M data)
           {
               Data = data;
           }

           public Node<M> getnext()
           {
               return next;
           }

           public void setnext(Node<M> next)
           {
               this.Next = next;
           }

            public Node<M> getPrevious()
            {
                return prev;
            }

            public void setPrevious(Node<M> prev)
            {
                this.Prev = prev;
            }
    }

    public class Linked_List<T>  : IEnumerable<T> where T : class
    {

        Node<T> head;
        Node<T> tail;
        int count;

        public int Count { get => count;}

        public Linked_List()
        {
            this.head = null;
            this.tail = null;
            count = 0;
        }

        public bool IsEmpty()
        {
            return head == null;
        }

        public void Clear()
        {
            this.head = null;
            this.tail = null;
            count = 0;
        }

        public bool Contains(T Item)
        {
            Node<T> p = head;
            while (p != null)
            {
                if (p.getData() == Item)
                {
                    return true;
                }
                p = p.getnext();
            }

            return false;
        }

        private void Display()
        {
            Node<T> p = head;
            while (p != null)
            {
                Console.WriteLine(p.getData());
                p = p.getnext();
            }
        }

        public int IndexOf(T Item)
        {
            int index = -1, count = 0;

            Node<T> P = head;
            while (P != null)
            {
                if (Item == P.getData())
                    return count;

                P = P.getnext();
                count++;
            }

            return index;
        }

        public void Insert(T Item)//Inserts an instance in Data Structure 
        {
            Node<T> N = new Node<T>(Item);
            if (Count == 0)
            {
                InsertAtStart(Item);
                return;
            }

            if (tail != null)
            {
                N.setPrevious(tail);
                tail.setnext(N);
            }
            tail = N;
            count++;
        }

        public void InsertAtStart(T Item)
        {
            Node<T> N = new Node<T>(Item);

            if (head != null)
            {
                N.setnext(head);
                head.setPrevious(N);
            }
            else
            {
                tail = N;
            }
            head = N;
            count++;
        }

        public void InsertAt(int index, T Item)
        {          
            if (index == 0)
                InsertAtStart(Item);

            else if (index == Count)
                Insert(Item); // Inserts At end

            else if (index < Count)
            {
                Node<T> N = new Node<T>(Item);
                Node<T> Current = head;

                int counter = 0;
                while (counter < index)
                {
                    Current = Current.getnext();
                    counter++;
                }

                Current.getPrevious().setnext(N);
                N.setPrevious(Current.getPrevious());

                N.setnext(Current);
                Current.setPrevious(N);
                count++;
            }

            else
                throw new IndexOutOfRangeException();
        }

        //bool Update(T NewKey, int Location)
        //{
        //    int lstLength = Length();
        //    if (lstLength > 0 && Location < lstLength)
        //    {
        //        Node<T>* P = head;
        //        int count = 0;
        //        while (count < Location)
        //        {
        //            P = P->getnext();
        //            count++;
        //        }

        //        P->setData(NewKey);
        //        return true;
        //    }
        //    return false;
        //}

        private int Length()
        {
            Node<T> p = head;
            int count = 0;
            while (p != null)
            {
                p = p.getnext();
                count++;
            }

            return count;
        }

        public void Remove(T Item)
        {
            Node<T> P = head, Prev = null;
            while (P != null)
            {
                if (Item == P.getData())
                {
                    if (Prev != null)
                        Prev.setnext(P.getnext());
                    
                    else
                        head = head.getnext();

                    count--;
                    break;
                }
                Prev = P;
                P = P.getnext();
            }
        }

        public void RemoveFirstItem()
        {
            if (head != null)
                head = head.getnext();

            count--;
        }

        public void RemoveLastItem()
        {
            if (tail == null)
                return;

            if (head == tail)
            {
                head = null;
                tail = null;
            }

            else
            {
                tail.getPrevious().setnext(null);
                tail = tail.getPrevious();

                // Node<T> P = head;
                // while (P != null)
                // {
                //     if (P.getnext().getnext() == null)
                //     {
                //         P.setnext(null);
                //         break;
                //     }
                //     P = P.getnext();
                // }
            }

            count--;
        }

        public void Reverse()
        {
            Node<T> Current = head.getnext(), P = head;
            Node<T> Prev = head;
            while (Current != null)
            {
                Node<T> temp = Current.getnext();
                Prev.setnext(Current.getnext());
                Current.setnext(head);
                head = Current;

                Current = temp;
            }
        }

        public List<T> ToList()
        {
            List<T> linkedList = new List<T>();
            Node<T> current = head;
            while (current != null)
            {
                linkedList.Add(current.Data);
                current = current.Next;
            }

            return linkedList;
        }

        public IEnumerator<T> GetEnumerator()
        {
            Node<T> current = head;
            while (current != null)
            {
                yield return current.Data;
                current = current.Next;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
