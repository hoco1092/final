﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distribution_Managment_System.BL
{
    public class RegionsofDMS
    {
        public RegionsofDMS() { }

        public RegionsofDMS(string name)
        {
            Name = name;
        }

        string name;

        public string Name { get => name; set => name = value; }
    }
}
