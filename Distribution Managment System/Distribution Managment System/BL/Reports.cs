﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Distribution_Managment_System.Data_Logic;
using DevExpress.Utils;
using CsvHelper;
using System.Globalization;
using System.IO;
using DevExpress.CodeParser;
using static DevExpress.Data.Filtering.Helpers.SubExprHelper;
using Distribution_Managment_System.BL;
using DevExpress.ClipboardSource.SpreadsheetML;

namespace Distribution_Managment_System.BL
{
    enum Reports
    {
        [Display(Name = "Sales Report")]
        Sales_Report,
        [Display(Name = "Top Sellers Report")]
        Top_Sellers_Report,
        /*[Display(Name = "Order Fullfillment Report")]
        Order_Fullfillment_Report,
        [Display(Name = "Dead Stock Report")]
        Dead_Stock_Report,*/
        [Display(Name = "Fuel Report")]
        Fuel_Report,
    }
    public class SaleReportData
    {
        string region;
        float revenue;
        float sales;
        float percentageProfit;

        public SaleReportData(string region, float revenue, float sales, float percentageProfit)
        {
            this.Region = region;
            this.Revenue = revenue;
            this.Sales = sales;
            this.PercentageProfit = percentageProfit;
        }

        public string Region { get => region; set => region = value; }
        public float Revenue { get => revenue; set => revenue = value; }
        public float Sales { get => sales; set => sales = value; }
        public float PercentageProfit { get => percentageProfit; set => percentageProfit = value; }
    }
    public class TopSeller
    {
        string region;
        float revenue;
        float sales;
        string product;
        int quantity;

        public TopSeller(string region, float revenue, float sales, int quantity,string product)
        {
            this.Region = region;
            this.Revenue = revenue;
            this.Sales = sales;
            this.Quantity = quantity;
            this.Product = product;
        }

        public string Region { get => region; set => region = value; }
        public float Revenue { get => revenue; set => revenue = value; }
        public float Sales { get => sales; set => sales = value; }
        public int Quantity { get => quantity; set => quantity = value; }
        public string Product { get => product; set => product = value; }
    }
    class SalesReportDataCRUD
    {
        static List<SaleReportData> saleReportDetailList = new List<SaleReportData>();

        internal static List<SaleReportData> SaleReportDatas { get => saleReportDetailList; set => saleReportDetailList = value; }
        public static IList<SaleReportData> getData()
        {
            return saleReportDetailList;
        }
        public static void makeList()
        {
            foreach (string r in Enum.GetNames(typeof(Regions)))
            {
                saleReportDetailList.Add(new SaleReportData(r, 0f, 0f, 0f));
            }
        }
        public static void WriteinCSV()
        {
            using (var textWriter = new StreamWriter("SalesReport.csv"))
            {
                string path = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                var writer = new CsvWriter(textWriter, CultureInfo.InvariantCulture);

                writer.WriteHeader<SaleReportData>();
                writer.NextRecord();
                foreach (SaleReportData data in SaleReportDatas)
                {
                    writer.WriteField(data.Region);
                    writer.WriteField(data.Revenue);
                    writer.WriteField(data.Sales);
                    writer.WriteField(data.PercentageProfit);
                    writer.NextRecord();
                }
                textWriter.Close();
            }
        }
    }
    class TopSellerDataCRUD
    {
        static List<TopSeller> TopSellerDataList = new List<TopSeller>();

        public static List<TopSeller> TopSellersDatalst { get => TopSellerDataList; set => TopSellerDataList = value; }

        public static IList<TopSeller> getData()
        {
            return TopSellersDatalst;
        }
        public static void makeList()
        {
            foreach (string r in Enum.GetNames(typeof(Regions)))
            {
                TopSellersDatalst.Add(new TopSeller(r, 0f, 0f, 0,""));
            }
        }
        public static void WriteinCSV()
        {
            using (var textWriter = new StreamWriter("TopSeller.csv"))
            {
                var writer = new CsvWriter(textWriter, CultureInfo.InvariantCulture);

                writer.WriteHeader<TopSeller>();
                writer.NextRecord();
                foreach (TopSeller data in TopSellersDatalst)
                {
                    writer.WriteField(data.Region);
                    writer.WriteField(data.Revenue);
                    writer.WriteField(data.Sales);
                    writer.WriteField(data.Quantity);
                }
                writer.NextRecord();
            }
        }
    }

    class ReportsGeneration
        {
        public static void SalesReport(DateTime start, DateTime End)
        {
            float totalSales = 0f, totalRevenue = 0f;
            List<Order> ordersinBtwDate = new List<Order>();
            foreach (Order ord in CRUD_Orders.GetOrderList())
            {
                if (ord.IsBtwTimeInterval(start, End))
                {
                    ordersinBtwDate.Add(ord);
                }
            }
            SalesReportDataCRUD.makeList();
            foreach (SaleReportData data in SalesReportDataCRUD.SaleReportDatas)
            {
                foreach (Order ord in ordersinBtwDate)
                {
                    if (Shops_CRUD.regionbyshopID(ord.ShopId) == data.Region)
                    {
                        float profitBYBill = ord.getProfit();
                        totalSales += ord.Bill;
                        totalRevenue += profitBYBill;
                        data.Sales += ord.Bill;
                        data.Revenue += profitBYBill;
                    }
                }
                data.PercentageProfit += data.Revenue / totalRevenue * 100;
            }
            SalesReportDataCRUD.WriteinCSV();
        }
        public static void TopSellerReport(DateTime start, DateTime End)
        {
            Dictionary<string, int> itemCount = new Dictionary<string, int>();
            List<Order> ordersinBtwDate = new List<Order>();
            foreach (Order ord in CRUD_Orders.GetOrderList())
            {
                if (ord.IsBtwTimeInterval(start, End))
                {
                    ordersinBtwDate.Add(ord);
                }
            }
            TopSellerDataCRUD.makeList();
            foreach (TopSeller data in TopSellerDataCRUD.TopSellersDatalst)
            {
                foreach (Order ord in ordersinBtwDate)
                {
                    foreach (OrderLine oneOrd in ord.OrderLineList)
                    {
                        if (Shops_CRUD.regionbyshopID(ord.ShopId) == data.Region)
                        {
                            if (itemCount.ContainsKey(oneOrd.ProductName))
                            {
                                itemCount[oneOrd.ProductName] += oneOrd.QuantityGiven;
                            }
                            else
                            {
                                itemCount[oneOrd.ProductName] = oneOrd.QuantityGiven;
                            }
                        }
                    }
                }
                 var sortedItems = from item in itemCount orderby item.Value descending select item;
                if(sortedItems!=null)
                { 
                data.Quantity = sortedItems.ElementAt(0).Value;
                data.Product = sortedItems.ElementAt(0).Key;
                }
                else
                {
                    data.Quantity = 0;
                    data.Product = "";
                }
                Product pro = Products_CRUD.GetProdByName(data.Product);
                if (pro!=null)
                {
                    data.Sales=Products_CRUD.GetProdByName(data.Product).SalePrice * data.Quantity;
                    data.Revenue= Products_CRUD.GetProdByName(data.Product).getProfit()* data.Quantity;
                }
                else
                {
                    data.Sales = 0f;
                    data.Revenue = 0f;
                }
            }

        }
    }
}
