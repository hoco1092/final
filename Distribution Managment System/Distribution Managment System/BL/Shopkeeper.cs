﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distribution_Managment_System.BL
{
    class Shopkeeper
    {
        private string ShopkeeperID;
        private string ShopkeeperName;
        private string Phonenum;
        private string shopkeeper_email;

        public static List<Shop> ShopsOwned = new List<Shop>();

        public Shopkeeper(string ShopkeeperName, string shopkeeper_email, string Phonenum)
        {
            this.ShopkeeperName = ShopkeeperName;
            this.Shopkeeper_email = shopkeeper_email;
            this.Phonenum = Phonenum;

        }

        public string ShopkeeperID1 { get => ShopkeeperID; set => ShopkeeperID = value; }
        public string ShopkeeperName1 { get => ShopkeeperName; set => ShopkeeperName = value; }
        public string Phonenum1 { get => Phonenum; set => Phonenum = value; }

        public string Shopkeeper_email { get => shopkeeper_email; set => shopkeeper_email = value; }
        static List<Shop> ShopsOwned1 { get => ShopsOwned; set => ShopsOwned = value; }


        public static void AddShop(Shop shop)
        {
            ShopsOwned.Add(shop);
        }







        public static void UpdateShop(Shop oldshop, Shop newshop)
        {

            foreach (Shop shops in ShopsOwned.ToList())
            {
                if (shops == oldshop)
                {
                    oldshop.ShopID1 = newshop.ShopID1;
                    oldshop.ShopName1 = newshop.ShopName1;
                    oldshop.Region1 = newshop.Region1;
                    oldshop.Landline1 = newshop.Landline1;
                    oldshop.Address1 = newshop.Address1;
                }
            }
        }

        public static bool DeleteShop(String id)
        {
            foreach (Shop shops in ShopsOwned.ToList())
            {
                if (id == shops.ShopID1)
                {
                    ShopsOwned.Remove(shops);
                }
            }
            return false;
        }


    }
}
