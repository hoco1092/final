﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace avl
{
    public class Node<T> 
    {
        public T data;
        public Node<T> l;
        public Node<T> r;

        public Node(T data)
        {
            this.data = data;
        }
    }

    public class AvlTree<T> where T : IComparable<T>,IEquatable<T>
    {
        

        Node<T> root;
        
        public void insert(T input)
        {
            Node<T> inserted_node = new Node<T>(input);
            if (root==null)
            {
                root = inserted_node;
            }
            else
            {
                root=Insertv2(root,inserted_node);
            }
        }

        public Node<T> Insertv2(Node<T> root, Node<T> inserted_node) 
        {
            if(root==null)
            {
                root = inserted_node;
            }
            else if(inserted_node.data.CompareTo(root.data)<0)
            {
                root.l=Insertv2(root.l,inserted_node);
                root = balance(root);
            }
            else if (inserted_node.data.CompareTo(root.data)>0)
            {
                root.r = Insertv2(root.r, inserted_node);
                root = balance(root);
            }

            return root;
        }

        public Node<T> balance(Node<T> root)
        {
            T balance = calculate_balance(root);
            if(balance.CompareTo((T)(Object)1)>0)
            {
                if(calculate_balance(root.l).CompareTo((T)(Object)0)>0)
                {
                    root = LLrotate(root);
                }
                else
                {
                    root = LRrotate(root);
                }
            }

            else if(balance.CompareTo((T)(Object)(-1))<0)
            {
                if(calculate_balance(root.r).CompareTo((T)(Object)0)>0)
                {
                    root = RLrotate(root);
                }
                else
                {
                    root = RRrotate(root);
                }
            }
            return root;
        }

        public T calculate_balance(Node<T> root)
        {
            T l = height(root.l);
            T r = height(root.r);
            dynamic L = l;
            dynamic R = r;
            T b;
            if (typeof(T) == typeof(int))
            {
                b = L - R;
            }
            else if (typeof(T) == typeof(string))
            {
                //L =Int32.Parse((T)(Object)l);

                b = L - R;
            }
            else
            {
                b = L - R;
            }
            //T b = L - R;
            return b;
        }

        public Node<T> RLrotate(Node<T> root)
        {
            Node<T> p = root.r;
            root.r = LLrotate(p);
            return RRrotate(root);
        }

        public Node<T> LRrotate(Node<T> root)
        {
            Node<T> p = root.l;
            root.l=RRrotate(p);
            return LLrotate(root);
        }

        public Node<T> LLrotate(Node<T> root)
        {
            Node<T> p = root.l;
            root.l=p.r;
            p.r = root;
            return p;
        }

        public Node<T> RRrotate(Node<T> root)
        {
            Node<T> p = root.r;
            root.r = p.l;
            p.l = root;
            return p;
        }

        public T search(T n)
        {
            if(Equals((retrieve(root,n).data),n))
            {
                return n;
            }
            else
            {
                if (typeof(T) == typeof(int))
                {
                    return (T)(Object)(-1);
                }
                else if (typeof(T) == typeof(string))
                {
                    return (T)(Object)"Not found";
                }
                else
                {
                    return (T)(Object)(-1);
                }
            }
        }

        public Node<T> retrieve(Node<T> root, T n)
        {
            if(root==null)
            {
                return null;
            }
            else if(Equals((root.data),n))
            {
                return root;
            }
            else if(n.CompareTo(root.data)<0)
            {
                return retrieve(root.l, n);
            }
            else if(n.CompareTo(root.data)>0)
            {
                return retrieve(root.r, n);
            }

            return root;
        }

        public void showtree()
        {
            if (root == null)
            {
                Console.WriteLine("Tree empty");
                return;
            }
            else
            {
                Inordertraversal(root);
            }

        }

        public void Inordertraversal(Node<T> root)
        {
            if(root!=null)
            {
                Inordertraversal(root.l);
                Console.WriteLine(root.data);
                Inordertraversal(root.r);
            }
        }

        public T getmax(T l, T r)
        {
            return l.CompareTo(r) > 0 ? l : r;
        }

        public T height(Node<T> root)
        {
            T h;

            if (typeof(T) == typeof(int))
            {
                 h = (T)(Object)0;
            }
            else if (typeof(T) == typeof(string))
            {
                 h = (T)(Object)"";
            }
            else if (typeof(T) == typeof(Object))
            {
                h = (T)(Object)null;
            }
            else
            {
                h = (T)(Object)0;
            }


            if (root!=null)
            {
                T l = height(root.l);
                T r = height(root.r);
                T x=getmax(l, r);
                dynamic H = h;
                dynamic X = x;
                if (typeof(T) == typeof(int))
                {
                    H = X + (T)(Object)1;
                }
                else if (typeof(T) == typeof(string))
                {
                    H = X + (T)(Object)1.ToString();
                }
                else
                {
                    H = X + (T)(Object)1;
                }
            }
            return h;
        }

        public void delete(T n)
        {
            root = remove(root,n);
        }

        public Node<T> remove(Node<T> root,T n)
        {
            Node<T> p;
            if(root==null)
            {
                return null;
            }
            else
            {
                if(n.CompareTo(root.data)<0)
                {
                    root.l = remove(root.l, n);
                    if(Equals(calculate_balance(root),-2))
                    {
                        if(calculate_balance(root.r).CompareTo((T)(Object)0)<=0)
                        {
                            root=RRrotate(root);
                        }
                        else
                        {
                            root = RLrotate(root);
                        }
                    }
                }
                else if(n.CompareTo(root.data)>0)
                {
                    root.r = remove(root.r, n);
                    if(Equals(calculate_balance(root), -2))
                    {
                        if(calculate_balance(root.l).CompareTo((T)(Object)0)>=0)
                        {
                            root = LLrotate(root);
                        }
                        else
                        {
                            root=LRrotate(root);
                        }    
                    }
                }
                else
                {
                    if (root.r != null)
                    {
                       
                        p = root.r;
                        while (p.l != null)
                        {
                            p = p.l;
                        }
                        root.data = p.data;
                        root.r = remove(root.r,p.data);
                        if (Equals(calculate_balance(root),2))
                        {
                            if (calculate_balance(root.l).CompareTo((T)(Object)0) >= 0)
                            {
                                root = LLrotate(root);
                            }
                            else 
                            { 
                                root = LRrotate(root); 
                            }
                        }
                    }
                    else
                    {   
                        return root.l;
                    }
                }
            }
            return root;

        }
    
    }

   

}
