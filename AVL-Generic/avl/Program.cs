﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace avl
{
    class Program
    {
        static void Main(string[] args)
        {
            /*AvlTree<string> tree = new AvlTree<string>();
            tree.insert("a");
            tree.insert("b");
            tree.insert("c");
            tree.insert("d");*/
            AvlTree<int> tree = new AvlTree<int>();
            tree.insert(1);
            tree.insert(2);
            tree.insert(3);
            tree.insert(4);
            tree.delete(2);
            tree.showtree();
            Console.ReadKey();
            
        }
    }

}
