﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace U13_15
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void Infoentering_panel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button8_Click(object sender, EventArgs e)
        {
            this.shpkep_btn.BackColor = ColorTranslator.FromHtml("#ba234d");
        }

        private void button9_Click(object sender, EventArgs e)
        {
            this.ordrdsh_btn.BackColor = ColorTranslator.FromHtml("#ba234d");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Order ID");
            dt.Columns.Add("Shop Name");
            dt.Columns.Add("Product");
            dt.Columns.Add("Quantity");
            dt.Columns.Add("Product ID");
            //foreach (var book in sortedList)
            //{
            //    dt.Rows.Add(book.getbookname(), book.getbookprice());

            //}
            orders_dt.DataSource = dt;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void Navigation_panel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            /*#if(orderid_txtbox.Text!= string.Empty || shopname_combox.SelectedValue!= null || product_combox.SelectedValue!= null || quantity_txtbox.Text!=string.Empty || int.Parse(quantity_txtbox.Text)<0)
            {
                Orderline order = new Orderline(orderid_txtbox.Text, shopname_combox.SelectedValue.ToString(), product_combox.SelectedValue.ToString(), int.Parse(quantity_txtbox.Text));
                order.Add(order);
                MessageBox.Show("Added");
            }
            else
            {
                DialogResult dialog = MessageBox.Show("Invalid Input,try again?", "Error", MessageBoxButtons.OK);
                if (dialog == DialogResult.OK)
                {
                    this.Close();
                }
            }*/
           
        }

        private void logout_btn_Click(object sender, EventArgs e)
        {
            this.logout_btn.BackColor = ColorTranslator.FromHtml("#ba234d");
        }

        private void logoutbtn_MouseEnter(object sender, EventArgs e)
        {
            this.logout_btn.BackColor = ColorTranslator.FromHtml("#db2154");
        }

        private void logout_btn_MouseLeave(object sender, EventArgs e)
        {
            this.logout_btn.BackColor = ColorTranslator.FromHtml("#33334c");
        }

        private void billhistory_btn_MouseEnter(object sender, EventArgs e)
        {
            this.billhistory_btn.BackColor = ColorTranslator.FromHtml("#db2154");
        }

        private void billhistory_btn_MouseLeave(object sender, EventArgs e)
        {
            this.billhistory_btn.BackColor = ColorTranslator.FromHtml("#33334c");
        }

        private void ordrdsh_btn_MouseEnter(object sender, EventArgs e)
        {
            this.ordrdsh_btn.BackColor = ColorTranslator.FromHtml("#db2154");
        }

        private void ordrdsh_btn_MouseLeave(object sender, EventArgs e)
        {
            this.ordrdsh_btn.BackColor = ColorTranslator.FromHtml("#33334c");
        }

        private void shpkep_btn_MouseEnter(object sender, EventArgs e)
        {
            this.shpkep_btn.BackColor = ColorTranslator.FromHtml("#db2154");
        }

        private void shpkep_btn_MouseLeave(object sender, EventArgs e)
        {
            this.shpkep_btn.BackColor = ColorTranslator.FromHtml("#33334c");
        }

        private void billhistory_btn_Click(object sender, EventArgs e)
        {
            this.billhistory_btn.BackColor = ColorTranslator.FromHtml("#ba234d");
        }

        private void orderid_txtbox_Enter(object sender, EventArgs e)
        {
            if(orderid_txtbox.Text=="ID")
            {
                orderid_txtbox.Text = "";
            }
            orderid_txtbox.ForeColor = Color.Black;
        }

        private void orderid_txtbox_Leave(object sender, EventArgs e)
        {
            if (orderid_txtbox.Text == "")
            {
                orderid_txtbox.Text = "ID";
            }
            orderid_txtbox.ForeColor = Color.Silver;

        }

        private void quantity_txtbox_Enter(object sender, EventArgs e)
        {
            if (quantity_txtbox.Text == "QUANTITY")
            {
                quantity_txtbox.Text = "";
            }
            quantity_txtbox.ForeColor = Color.Black;
        }

        private void quantity_txtbox_Leave(object sender, EventArgs e)
        {
            if (quantity_txtbox.Text == "")
            {
                quantity_txtbox.Text = "QUANTITY";
            }
            quantity_txtbox.ForeColor = Color.Silver;

        }

        private void product_combox_Enter(object sender, EventArgs e)
        {
            if (product_combox.Text == "PRODUCT")
            {
                product_combox.Text = "";
            }
            product_combox.ForeColor = Color.Black;

        }

        private void product_combox_Leave(object sender, EventArgs e)
        {
            if (product_combox.Text == "")
            {
                product_combox.Text = "PRODUCT";
            }
            product_combox.ForeColor = Color.Silver;
        }

        private void shopname_combox_Enter(object sender, EventArgs e)
        {
            if (shopname_combox.Text == "SHOP")
            {
                shopname_combox.Text = "";
            }
            shopname_combox.ForeColor = Color.Black;
        }

        private void shopname_combox_Leave(object sender, EventArgs e)
        {
            if (shopname_combox.Text == "")
            {
                shopname_combox.Text = "SHOP";
            }
            shopname_combox.ForeColor = Color.Silver;

        }

        private void textBox3_Enter(object sender, EventArgs e)
        {
            if (search_txtbox.Text == "SEARCH")
            {
                search_txtbox.Text = "";
            }
            search_txtbox.ForeColor = Color.Black;
        }

        private void search_txtbox_Leave(object sender, EventArgs e)
        {
            if (search_txtbox.Text == "")
            {
                search_txtbox.Text = "SEARCH";
            }
            search_txtbox.ForeColor = Color.Silver;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void CRUD_panel_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
