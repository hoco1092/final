﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace U13_15
{
    interface ICRUD
    {
        void Add();
        void Remove();
        void Update();
        void Search();

    }
}
