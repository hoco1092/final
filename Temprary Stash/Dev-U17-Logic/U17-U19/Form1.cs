﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace U13_15
{

    public partial class Form1 : Form
    {
        DataTable order_table = new DataTable("Order_table");
        Bitmap print;
        int idxrow;
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void Infoentering_panel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button8_Click(object sender, EventArgs e)
        {
            this.shpkep_btn.BackColor = ColorTranslator.FromHtml("#ba234d");
        }

        private void button9_Click(object sender, EventArgs e)
        {
            this.ordrdsh_btn.BackColor = ColorTranslator.FromHtml("#ba234d");
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            order_table.Columns.Add("Order ID");
            order_table.Columns.Add("Shop Name");
            order_table.Columns.Add("Product");
            order_table.Columns.Add("Quantity");
            order_table.Columns.Add("Product ID");
            orders_dt.DataSource = order_table;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void Navigation_panel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                if (orderid_txtbox.Text == string.Empty || shopname_combox.Text == string.Empty || product_combox.Text == string.Empty || quantity_txtbox.Text == string.Empty)
                {
                    MessageBox.Show("Invalid Input");
                }
                else if (int.Parse(quantity_txtbox.Text) < 0)
                {
                    MessageBox.Show("Ordered Items can't be negative");
                }
                else if (int.Parse(quantity_txtbox.Text) == 0)
                {
                    MessageBox.Show("Minimum Ordered Item should be 1");
                }
                else
                {
                    Orderline order = new Orderline(orderid_txtbox.Text, shopname_combox.Text, product_combox.Text, int.Parse(quantity_txtbox.Text));
                    order.Add(order);
                    MessageBox.Show("Order Added");
                    order_table.Rows.Add(orderid_txtbox.Text, shopname_combox.Text, product_combox.Text, int.Parse(quantity_txtbox.Text));
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message, "No Alphabets in Quantity");
            }

        }

        private void logout_btn_Click(object sender, EventArgs e)
        {
            this.logout_btn.BackColor = ColorTranslator.FromHtml("#ba234d");
        }

        private void logoutbtn_MouseEnter(object sender, EventArgs e)
        {
            this.logout_btn.BackColor = ColorTranslator.FromHtml("#db2154");
        }

        private void logout_btn_MouseLeave(object sender, EventArgs e)
        {
            this.logout_btn.BackColor = ColorTranslator.FromHtml("#33334c");
        }

        private void billhistory_btn_MouseEnter(object sender, EventArgs e)
        {
            this.billhistory_btn.BackColor = ColorTranslator.FromHtml("#db2154");
        }

        private void billhistory_btn_MouseLeave(object sender, EventArgs e)
        {
            this.billhistory_btn.BackColor = ColorTranslator.FromHtml("#33334c");
        }

        private void ordrdsh_btn_MouseEnter(object sender, EventArgs e)
        {
            this.ordrdsh_btn.BackColor = ColorTranslator.FromHtml("#db2154");
        }

        private void ordrdsh_btn_MouseLeave(object sender, EventArgs e)
        {
            this.ordrdsh_btn.BackColor = ColorTranslator.FromHtml("#33334c");
        }

        private void shpkep_btn_MouseEnter(object sender, EventArgs e)
        {
            this.shpkep_btn.BackColor = ColorTranslator.FromHtml("#db2154");
        }

        private void shpkep_btn_MouseLeave(object sender, EventArgs e)
        {
            this.shpkep_btn.BackColor = ColorTranslator.FromHtml("#33334c");
        }

        private void billhistory_btn_Click(object sender, EventArgs e)
        {
            this.billhistory_btn.BackColor = ColorTranslator.FromHtml("#ba234d");
        }

        private void orderid_txtbox_Enter(object sender, EventArgs e)
        {
            if (orderid_txtbox.Text == "ID")
            {
                orderid_txtbox.Text = "";
            }
            orderid_txtbox.ForeColor = Color.Black;
        }

        private void orderid_txtbox_Leave(object sender, EventArgs e)
        {
            /*if (orderid_txtbox.Text == "")
            {
                orderid_txtbox.Text = "ID";
            }
            orderid_txtbox.ForeColor = Color.Silver;*/

        }

        private void quantity_txtbox_Enter(object sender, EventArgs e)
        {
            if (quantity_txtbox.Text == "QUANTITY")
            {
                quantity_txtbox.Text = "";
            }
            quantity_txtbox.ForeColor = Color.Black;
        }

        private void quantity_txtbox_Leave(object sender, EventArgs e)
        {
            /*if (quantity_txtbox.Text == "")
            {
                quantity_txtbox.Text = "QUANTITY";
            }
            quantity_txtbox.ForeColor = Color.Silver;*/

        }

        private void product_combox_Enter(object sender, EventArgs e)
        {
            if (product_combox.Text == "PRODUCT")
            {
                product_combox.Text = "";
            }
            product_combox.ForeColor = Color.Black;

        }

        private void product_combox_Leave(object sender, EventArgs e)
        {
            /* if (product_combox.Text == "")
             {
                 product_combox.Text = "PRODUCT";
             }
             product_combox.ForeColor = Color.Silver;*/
        }

        private void shopname_combox_Enter(object sender, EventArgs e)
        {
            if (shopname_combox.Text == "SHOP")
            {
                shopname_combox.Text = "";
            }
            shopname_combox.ForeColor = Color.Black;
        }

        private void shopname_combox_Leave(object sender, EventArgs e)
        {
            /*if (shopname_combox.Text == "")
            {
                shopname_combox.Text = "SHOP";
            }
            shopname_combox.ForeColor = Color.Silver;*/

        }

        private void textBox3_Enter(object sender, EventArgs e)
        {
            if (search_txtbox.Text == "SEARCH")
            {
                search_txtbox.Text = "";
            }
            search_txtbox.ForeColor = Color.Black;
        }

        private void search_txtbox_Leave(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void CRUD_panel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button12_Click(object sender, EventArgs e)
        {
            orderid_txtbox.Clear();
            shopname_combox.SelectedIndex = -1;
            product_combox.SelectedIndex = -1;
            quantity_txtbox.Clear();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int orders = Orderline.orderlist.Count;
            for (int i = 0; i < orders; i++)
            {
                order_table.Rows[i]["Product ID"] = "P000" + i;

            }
            //remove obj from list
        }

        private void orders_dt_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void del()
        {
            foreach(DataGridViewRow item in this.orders_dt.SelectedRows)
            {
                orders_dt.Rows.RemoveAt(item.Index);
            }

        }


        private void button6_Click(object sender, EventArgs e)
        {
            del();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            int h = orders_dt.Height;
            orders_dt.Height = orders_dt.RowCount * orders_dt.RowTemplate.Height * 2;
            print=new Bitmap(orders_dt.Width, orders_dt.Height);
            orders_dt.DrawToBitmap(print, new Rectangle(0, 0, orders_dt.Width, orders_dt.Height));
            printPreviewDialog1.PrintPreviewControl.Zoom = 1;
            printPreviewDialog1.ShowDialog();
            orders_dt.Height = h;
            
        }

        private void printDocument2_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {

        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            e.Graphics.DrawImage(print, 0, 0);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            save();
        }

        private void save()
        {
            Microsoft.Office.Interop.Excel._Application app= new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);
            Microsoft.Office.Interop.Excel._Worksheet worksheet = null;

            app.Visible = true;
            worksheet = workbook.Sheets["Sheet1"];
            worksheet = workbook.ActiveSheet;
            worksheet.Name = "Exported From Gridview";

            for (int i = 1; i < orders_dt.Columns.Count + 1; i++) 
            {
                worksheet.Cells[1, i] = orders_dt.Columns[i - 1].HeaderText;
            }

            for(int i=0;i<orders_dt.Rows.Count-1;i++)
            {
                for(int j=0;j<orders_dt.Columns.Count;j++) 
                {
                    worksheet.Cells[i + 2, j + 1] = orders_dt.Rows[i].Cells[j].Value.ToString();
                }
            }

        }
    }
}
