﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace U13_15
{
    class Orderline
    {
        private string orderid { get; set; }
        private string shopname { get; set; }
        private string product_type { get; set; }
        private string product_id { get; set; }
        private int quantity { get; set; }
        private float price { get; set; }
        public static List<Orderline> orderlist = new List<Orderline>();

        public Orderline(string orderid,string shopname,string product_type,int quantity)
        {
            this.orderid = orderid;
            this.shopname = shopname;  
            this.product_type = product_type;
            this.quantity = quantity;
        }
        

        public void Add(Orderline order)
        {
            orderlist.Add(order);
        }

        public void Remove(Orderline order)
        {
            orderlist.Remove(order);
        }


    }
}
