﻿namespace U13_15
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Navigation_panel = new System.Windows.Forms.Panel();
            this.billhistory_btn = new System.Windows.Forms.Button();
            this.logout_btn = new System.Windows.Forms.Button();
            this.ordrdsh_btn = new System.Windows.Forms.Button();
            this.shpkep_btn = new System.Windows.Forms.Button();
            this.Infoentering_panel = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.product_combox = new System.Windows.Forms.ComboBox();
            this.shopname_combox = new System.Windows.Forms.ComboBox();
            this.quantity_txtbox = new System.Windows.Forms.TextBox();
            this.orderid_txtbox = new System.Windows.Forms.TextBox();
            this.CRUD_panel = new System.Windows.Forms.Panel();
            this.orders_dt = new System.Windows.Forms.DataGridView();
            this.button2 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.add_btn = new System.Windows.Forms.Button();
            this.search_txtbox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.Navigation_panel.SuspendLayout();
            this.Infoentering_panel.SuspendLayout();
            this.CRUD_panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orders_dt)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // Navigation_panel
            // 
            this.Navigation_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(72)))));
            this.Navigation_panel.Controls.Add(this.billhistory_btn);
            this.Navigation_panel.Controls.Add(this.logout_btn);
            this.Navigation_panel.Controls.Add(this.ordrdsh_btn);
            this.Navigation_panel.Controls.Add(this.shpkep_btn);
            this.Navigation_panel.Location = new System.Drawing.Point(1, 102);
            this.Navigation_panel.Name = "Navigation_panel";
            this.Navigation_panel.Size = new System.Drawing.Size(163, 505);
            this.Navigation_panel.TabIndex = 0;
            this.Navigation_panel.Paint += new System.Windows.Forms.PaintEventHandler(this.Navigation_panel_Paint);
            // 
            // billhistory_btn
            // 
            this.billhistory_btn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(72)))));
            this.billhistory_btn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(72)))));
            this.billhistory_btn.FlatAppearance.BorderSize = 0;
            this.billhistory_btn.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.billhistory_btn.Location = new System.Drawing.Point(0, 57);
            this.billhistory_btn.Name = "billhistory_btn";
            this.billhistory_btn.Size = new System.Drawing.Size(163, 32);
            this.billhistory_btn.TabIndex = 11;
            this.billhistory_btn.Text = "BILL HISTORY";
            this.billhistory_btn.UseVisualStyleBackColor = false;
            this.billhistory_btn.Click += new System.EventHandler(this.billhistory_btn_Click);
            this.billhistory_btn.MouseEnter += new System.EventHandler(this.billhistory_btn_MouseEnter);
            this.billhistory_btn.MouseLeave += new System.EventHandler(this.billhistory_btn_MouseLeave);
            // 
            // logout_btn
            // 
            this.logout_btn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(72)))));
            this.logout_btn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(72)))));
            this.logout_btn.FlatAppearance.BorderSize = 0;
            this.logout_btn.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.logout_btn.Location = new System.Drawing.Point(0, 86);
            this.logout_btn.Name = "logout_btn";
            this.logout_btn.Size = new System.Drawing.Size(163, 31);
            this.logout_btn.TabIndex = 10;
            this.logout_btn.Text = "LOG OUT";
            this.logout_btn.UseVisualStyleBackColor = false;
            this.logout_btn.Click += new System.EventHandler(this.logout_btn_Click);
            this.logout_btn.MouseEnter += new System.EventHandler(this.logoutbtn_MouseEnter);
            this.logout_btn.MouseLeave += new System.EventHandler(this.logout_btn_MouseLeave);
            // 
            // ordrdsh_btn
            // 
            this.ordrdsh_btn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(72)))));
            this.ordrdsh_btn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(72)))));
            this.ordrdsh_btn.FlatAppearance.BorderSize = 0;
            this.ordrdsh_btn.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ordrdsh_btn.Location = new System.Drawing.Point(0, 33);
            this.ordrdsh_btn.Name = "ordrdsh_btn";
            this.ordrdsh_btn.Size = new System.Drawing.Size(163, 28);
            this.ordrdsh_btn.TabIndex = 9;
            this.ordrdsh_btn.Text = "ORDER DASHBOARD";
            this.ordrdsh_btn.UseVisualStyleBackColor = false;
            this.ordrdsh_btn.Click += new System.EventHandler(this.button9_Click);
            this.ordrdsh_btn.MouseEnter += new System.EventHandler(this.ordrdsh_btn_MouseEnter);
            this.ordrdsh_btn.MouseLeave += new System.EventHandler(this.ordrdsh_btn_MouseLeave);
            // 
            // shpkep_btn
            // 
            this.shpkep_btn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(72)))));
            this.shpkep_btn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(72)))));
            this.shpkep_btn.FlatAppearance.BorderSize = 0;
            this.shpkep_btn.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.shpkep_btn.Location = new System.Drawing.Point(0, 3);
            this.shpkep_btn.Name = "shpkep_btn";
            this.shpkep_btn.Size = new System.Drawing.Size(163, 33);
            this.shpkep_btn.TabIndex = 8;
            this.shpkep_btn.Text = "SHOPKEEPER DASHBOARD";
            this.shpkep_btn.UseVisualStyleBackColor = false;
            this.shpkep_btn.Click += new System.EventHandler(this.button8_Click);
            this.shpkep_btn.MouseEnter += new System.EventHandler(this.shpkep_btn_MouseEnter);
            this.shpkep_btn.MouseLeave += new System.EventHandler(this.shpkep_btn_MouseLeave);
            // 
            // Infoentering_panel
            // 
            this.Infoentering_panel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.Infoentering_panel.Controls.Add(this.button1);
            this.Infoentering_panel.Controls.Add(this.label5);
            this.Infoentering_panel.Controls.Add(this.label4);
            this.Infoentering_panel.Controls.Add(this.label3);
            this.Infoentering_panel.Controls.Add(this.label2);
            this.Infoentering_panel.Controls.Add(this.product_combox);
            this.Infoentering_panel.Controls.Add(this.shopname_combox);
            this.Infoentering_panel.Controls.Add(this.quantity_txtbox);
            this.Infoentering_panel.Controls.Add(this.orderid_txtbox);
            this.Infoentering_panel.Location = new System.Drawing.Point(168, 102);
            this.Infoentering_panel.Name = "Infoentering_panel";
            this.Infoentering_panel.Size = new System.Drawing.Size(658, 168);
            this.Infoentering_panel.TabIndex = 1;
            this.Infoentering_panel.Paint += new System.Windows.Forms.PaintEventHandler(this.Infoentering_panel_Paint);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(314, 17);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(95, 25);
            this.button1.TabIndex = 7;
            this.button1.Text = "Generate ID";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(16, 143);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 16);
            this.label5.TabIndex = 6;
            this.label5.Text = "Quantity";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(16, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 16);
            this.label4.TabIndex = 5;
            this.label4.Text = "Product";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(16, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Shop Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(16, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "Order ID";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // product_combox
            // 
            this.product_combox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.product_combox.ForeColor = System.Drawing.Color.Gray;
            this.product_combox.FormattingEnabled = true;
            this.product_combox.Items.AddRange(new object[] {
            "Product 1",
            "Product 2",
            "Product 3"});
            this.product_combox.Location = new System.Drawing.Point(136, 96);
            this.product_combox.Name = "product_combox";
            this.product_combox.Size = new System.Drawing.Size(153, 21);
            this.product_combox.TabIndex = 3;
            this.product_combox.Enter += new System.EventHandler(this.product_combox_Enter);
            this.product_combox.Leave += new System.EventHandler(this.product_combox_Leave);
            // 
            // shopname_combox
            // 
            this.shopname_combox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.shopname_combox.ForeColor = System.Drawing.Color.Gray;
            this.shopname_combox.FormattingEnabled = true;
            this.shopname_combox.Items.AddRange(new object[] {
            "Shop 1",
            "Shop 2",
            "Shop 3"});
            this.shopname_combox.Location = new System.Drawing.Point(136, 54);
            this.shopname_combox.Name = "shopname_combox";
            this.shopname_combox.Size = new System.Drawing.Size(153, 21);
            this.shopname_combox.TabIndex = 2;
            this.shopname_combox.Enter += new System.EventHandler(this.shopname_combox_Enter);
            this.shopname_combox.Leave += new System.EventHandler(this.shopname_combox_Leave);
            // 
            // quantity_txtbox
            // 
            this.quantity_txtbox.ForeColor = System.Drawing.Color.Gray;
            this.quantity_txtbox.Location = new System.Drawing.Point(136, 139);
            this.quantity_txtbox.Name = "quantity_txtbox";
            this.quantity_txtbox.Size = new System.Drawing.Size(153, 20);
            this.quantity_txtbox.TabIndex = 1;
            this.quantity_txtbox.Text = "QUANTITY";
            this.quantity_txtbox.Enter += new System.EventHandler(this.quantity_txtbox_Enter);
            this.quantity_txtbox.Leave += new System.EventHandler(this.quantity_txtbox_Leave);
            // 
            // orderid_txtbox
            // 
            this.orderid_txtbox.ForeColor = System.Drawing.Color.Gray;
            this.orderid_txtbox.Location = new System.Drawing.Point(136, 17);
            this.orderid_txtbox.Name = "orderid_txtbox";
            this.orderid_txtbox.Size = new System.Drawing.Size(153, 20);
            this.orderid_txtbox.TabIndex = 0;
            this.orderid_txtbox.Text = "ID";
            this.orderid_txtbox.Enter += new System.EventHandler(this.orderid_txtbox_Enter);
            this.orderid_txtbox.Leave += new System.EventHandler(this.orderid_txtbox_Leave);
            // 
            // CRUD_panel
            // 
            this.CRUD_panel.BackColor = System.Drawing.Color.White;
            this.CRUD_panel.Controls.Add(this.orders_dt);
            this.CRUD_panel.Controls.Add(this.button2);
            this.CRUD_panel.Controls.Add(this.button12);
            this.CRUD_panel.Controls.Add(this.add_btn);
            this.CRUD_panel.Controls.Add(this.search_txtbox);
            this.CRUD_panel.Location = new System.Drawing.Point(170, 287);
            this.CRUD_panel.Name = "CRUD_panel";
            this.CRUD_panel.Size = new System.Drawing.Size(655, 288);
            this.CRUD_panel.TabIndex = 2;
            this.CRUD_panel.Paint += new System.Windows.Forms.PaintEventHandler(this.CRUD_panel_Paint);
            // 
            // orders_dt
            // 
            this.orders_dt.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.orders_dt.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.orders_dt.Location = new System.Drawing.Point(3, 32);
            this.orders_dt.Name = "orders_dt";
            this.orders_dt.ReadOnly = true;
            this.orders_dt.Size = new System.Drawing.Size(649, 253);
            this.orders_dt.TabIndex = 19;
            this.orders_dt.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.orders_dt_CellClick);
            this.orders_dt.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(214, 0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 26);
            this.button2.TabIndex = 18;
            this.button2.Text = "SEARCH";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button12
            // 
            this.button12.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.Location = new System.Drawing.Point(489, 0);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(75, 26);
            this.button12.TabIndex = 16;
            this.button12.Text = "CLEAR";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // add_btn
            // 
            this.add_btn.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.add_btn.Location = new System.Drawing.Point(580, 0);
            this.add_btn.Name = "add_btn";
            this.add_btn.Size = new System.Drawing.Size(75, 26);
            this.add_btn.TabIndex = 17;
            this.add_btn.Text = "ADD";
            this.add_btn.UseVisualStyleBackColor = true;
            this.add_btn.Click += new System.EventHandler(this.button4_Click);
            // 
            // search_txtbox
            // 
            this.search_txtbox.ForeColor = System.Drawing.Color.Gray;
            this.search_txtbox.Location = new System.Drawing.Point(19, 0);
            this.search_txtbox.Name = "search_txtbox";
            this.search_txtbox.Size = new System.Drawing.Size(153, 20);
            this.search_txtbox.TabIndex = 8;
            this.search_txtbox.Text = "SEARCH";
            this.search_txtbox.Enter += new System.EventHandler(this.textBox3_Enter);
            this.search_txtbox.Leave += new System.EventHandler(this.search_txtbox_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.Desktop;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(168, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(156, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Add An Order";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(519, 581);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 26);
            this.button5.TabIndex = 11;
            this.button5.Text = "PRINT";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(600, 581);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(110, 26);
            this.button6.TabIndex = 12;
            this.button6.Text = "CLEAR ORDER";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Location = new System.Drawing.Point(716, 581);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(110, 26);
            this.button7.TabIndex = 13;
            this.button7.Text = "SAVE ORDER";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(19)))), ((int)(((byte)(49)))));
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(1, -4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(161, 110);
            this.panel1.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(33, 37);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 36);
            this.label6.TabIndex = 16;
            this.label6.Text = "D M S";
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(167, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(658, 70);
            this.panel3.TabIndex = 15;
            // 
            // panel2
            // 
            this.panel2.Location = new System.Drawing.Point(167, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(645, 73);
            this.panel2.TabIndex = 15;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(28)))), ((int)(((byte)(70)))));
            this.panel4.Controls.Add(this.label8);
            this.panel4.Location = new System.Drawing.Point(168, -1);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(658, 73);
            this.panel4.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(171, 10);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(291, 36);
            this.label8.TabIndex = 17;
            this.label8.Text = "ORDER DASHBOARD";
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(173, 581);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 26);
            this.button3.TabIndex = 16;
            this.button3.Text = "EDIT";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Document = this.printDocument1;
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(834, 619);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CRUD_panel);
            this.Controls.Add(this.Infoentering_panel);
            this.Controls.Add(this.Navigation_panel);
            this.Name = "Form1";
            this.Text = "DMS";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Navigation_panel.ResumeLayout(false);
            this.Infoentering_panel.ResumeLayout(false);
            this.Infoentering_panel.PerformLayout();
            this.CRUD_panel.ResumeLayout(false);
            this.CRUD_panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orders_dt)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel Navigation_panel;
        private System.Windows.Forms.Panel Infoentering_panel;
        private System.Windows.Forms.Panel CRUD_panel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox product_combox;
        private System.Windows.Forms.ComboBox shopname_combox;
        private System.Windows.Forms.TextBox quantity_txtbox;
        private System.Windows.Forms.TextBox orderid_txtbox;
        private System.Windows.Forms.TextBox search_txtbox;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button billhistory_btn;
        private System.Windows.Forms.Button logout_btn;
        private System.Windows.Forms.Button ordrdsh_btn;
        private System.Windows.Forms.Button shpkep_btn;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button add_btn;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView orders_dt;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        private System.Drawing.Printing.PrintDocument printDocument1;
    }
}

